// Copyright Epic Games, Inc. All Rights Reserved.

#include "IJivanaGameSettings.h"

#if WITH_EDITOR
	#include "Modules/ModuleManager.h"
	#include "ISettingsModule.h"
	#include "ISettingsSection.h"
	#include "ISettingsContainer.h"
#endif

#define LOCTEXT_NAMESPACE "Jivana"

class FJivanaGameSettings : public IJivanaGameSettings
{
private:

	bool handleSettingsSaved()
	{
#if WITH_EDITOR
		const auto& Settings = GetMutableDefault<UJivanaGameSettings>();
		Settings->SaveConfig();
		return true;
#endif // WITH_EDITOR
		return false;
	}

	void registerSettings()
	{
#if WITH_EDITOR
		if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
		{
			ISettingsContainerPtr SettingsContainer = SettingsModule->GetContainer("Project");

			ISettingsSectionPtr SettingsSection = SettingsModule->RegisterSettings("Project", "Game", "JivanaGameSettings",
				LOCTEXT("JivanaGameSettings", "Jivana Settings"),
				LOCTEXT("JivanaGameSettingsDescription", "Settings for Jivana"),
			GetMutableDefault<UJivanaGameSettings>());

			if (SettingsSection.IsValid())
			{
				SettingsSection->OnModified().BindRaw(this, &FJivanaGameSettings::handleSettingsSaved);
			}
		}
#endif // WITH_EDITOR
	}

	void unregisterSettings()
	{
#if WITH_EDITOR
		if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
		{
			SettingsModule->UnregisterSettings("Project", "Game", "JivanaGameSettings");
		}
#endif // WITH_EDITOR
	}

public:

	virtual void StartupModule() override
	{
		this->registerSettings();
	}
	

	virtual void ShutdownModule() override
	{
		if (UObjectInitialized())
		{
			this->unregisterSettings();
		}
	}
};

#undef LOCTEXT_NAMESPACE

IMPLEMENT_GAME_MODULE(FJivanaGameSettings, JivanaGameSettings);
