/// Copyright 2020 (C) Jivana Team

using UnrealBuildTool;
using System.IO;

public class JivanaGameSettings : ModuleRules
{
	public JivanaGameSettings(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"Engine",
				"CoreUObject"
			}
		);

		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "Public"));
		PrivateIncludePaths.Add(Path.Combine(ModuleDirectory, "Private"));
	}
}
