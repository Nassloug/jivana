#pragma once

#include "JivanaGameSettings.h"

#include "Modules/ModuleManager.h"

class IJivanaGameSettings : public IModuleInterface
{
public:
	static inline IJivanaGameSettings& Get()
	{
		return FModuleManager::LoadModuleChecked<IJivanaGameSettings>("JivanaGameSettings");
	}

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("JivanaGameSettings");
	}
};
