#pragma once

#include "CoreMinimal.h"

#include "JivanaGameSettings.generated.h"

UCLASS(classGroup = "JivanaGameSettings", Category = "Game", Config = "Jivana")
class JIVANAGAMESETTINGS_API UJivanaGameSettings : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(Category = "GameSettings", EditDefaultsOnly, config)
		TSubclassOf<AActor> CheckpointActor;

	UJivanaGameSettings()
	{
		CheckpointActor = AActor::StaticClass();

		LoadConfig(); // <-- never forget to do this!
	}
};
