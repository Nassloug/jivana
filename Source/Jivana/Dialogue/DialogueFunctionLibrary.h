// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "../Jivana.h"
#include "../DataTables/PNJDialogueRow.h"
#include "../DataTables/PNJDetailRow.h"
#include "DialogueFunctionLibrary.generated.h"

DECLARE_DYNAMIC_DELEGATE(FDialogueEventDelegate);

USTRUCT(BlueprintType)
struct FDialogueEvent {
	GENERATED_USTRUCT_BODY()
public:
	FDialogueEventDelegate Delegate;
};

/**
 * 
 */
UCLASS()
class JIVANA_API UDialogueFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	/// FUNCTIONS ///

public:
	
	/**
	 *	Register a LinearColor mapping used in dialogue.
	 *
	 *	@param		Key				FString Key in dialogue
	 *	@param		Color			FLinearColor Color to display
	 *	@param		TMap			Color mapping map to store and use for dialogues
	 */
	UFUNCTION(BlueprintCallable, Category = "Dialogue")
		static void RegisterColorMapping(const FString& Key, const FLinearColor& Color, TMap<FString, FLinearColor>& ColorMapping);

	/**
	 *	Register an Event mapping used in dialogue.
	 *
	 *	@param		Key				FString Key in dialogue
	 *	@param		Callback		FDialogueEventDelegate Delegate to call on event
	 *	@param		TMap			Event mapping map to store and use for dialogues
	 */
	UFUNCTION(BlueprintCallable, Category = "Dialogue")
		static void RegisterEventMapping(const FString& Key, const FDialogueEventDelegate& Callback, TMap<FString, FDialogueEvent>& EventMapping);

	UFUNCTION(BlueprintCallable, Category = "Dialogue", meta = (WorldContext = "WorldContextObject"))
		static bool IsTimerHandleValid(const UObject* WorldContextObject, const FTimerHandleContainerWrapper& wrapper);

	UFUNCTION(BlueprintCallable, Category = "Dialogue", meta = (WorldContext = "WorldContextObject"))
		static void ClearTimerHandle(const UObject* WorldContextObject, const FTimerHandleContainerWrapper& wrapper);

	UFUNCTION(BlueprintCallable, Category = "Dialogue")
		static FPNJDetailRow& GetPNJDialogueData(const FPNJDialogueRow& pnjDialogue);
};
