// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueFunctionLibrary.h"

void UDialogueFunctionLibrary::RegisterColorMapping(const FString& Key, const FLinearColor& Color, TMap<FString, FLinearColor>& ColorMapping)
{
	static TMap<FString, FLinearColor> Mapping;

	Mapping.Add(Key, Color);
	ColorMapping = Mapping;
}

void UDialogueFunctionLibrary::RegisterEventMapping(const FString& Key, const FDialogueEventDelegate& Callback, TMap<FString, FDialogueEvent>& EventMapping)
{
	static TMap<FString, FDialogueEvent> Mapping;

	FDialogueEvent evt;
	evt.Delegate = Callback;

	Mapping.Add(Key, evt);
	EventMapping = Mapping;
}

bool UDialogueFunctionLibrary::IsTimerHandleValid(const UObject* WorldContextObject, const FTimerHandleContainerWrapper& wrapper)
{
	return wrapper.ptr.IsValid() && wrapper.ptr->isJivValid;
}

void UDialogueFunctionLibrary::ClearTimerHandle(const UObject* WorldContextObject, const FTimerHandleContainerWrapper& wrapper)
{
	auto& w = const_cast<FTimerHandleContainerWrapper&>(wrapper);
	w.ptr->isJivValid = false;
	w.ptr = nullptr;
	UE_LOG(LogTemp, Log, TEXT("CLEAR TIMER HANDLE"));
}

FPNJDetailRow& UDialogueFunctionLibrary::GetPNJDialogueData(const FPNJDialogueRow& pnjDialogue)
{
	return *pnjDialogue.PNJ.GetRow<FPNJDetailRow>("PNJDialogue");
}
