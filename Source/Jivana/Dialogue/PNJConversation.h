// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Jivana.h"
#include "GameFramework/Pawn.h"
#include "../PNJ/PNJBase.h"
#include "PNJConversation.generated.h"

//#define JIV_CAMERA_TEST

UCLASS()
class JIVANA_API APNJConversation : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APNJConversation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void PossessedBy(AController* NewController) override;

private:
	void PassDialogue();

	UFUNCTION()
		void OnTriggerOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnTriggerEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void DisplayDialogue(struct FPNJDialogueRow* const dialogue);

	void OnEndDialogue();

public:
	UPROPERTY(Category = "Dialogue", EditAnywhere, BlueprintReadWrite)
		class USphereComponent* DialogueTriggerRange;

	UPROPERTY(Category = "Dialogue", EditAnywhere, BlueprintReadWrite)
		TArray<APNJBase*> PNJAffected;

	UPROPERTY(Category = "Dialogue", EditAnywhere, BlueprintReadWrite)
		const class UDataTable* DataTable;

	UPROPERTY(Category = "Dialogue", EditAnywhere, BlueprintReadWrite)
		bool CallPlayer = false;

private:
	class APlayerBase* Player;

#ifdef JIV_CAMERA_TEST
	class UChildActorComponent* DialogueCameraChild;
	class AContextualCamera* DialogueCamera;
#else
	class UCameraComponent* DialogueCamera;
#endif

	int32 DialogueRowIndex;

	bool IsDialogueStarted;
};
