// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueDisplayNode.h"

#include "Components/TextBlock.h"
#include "Components/RichTextBlock.h"
#include "Blueprint/WidgetTree.h"
#include "Components/WrapBoxSlot.h"
#include "Components/WrapBox.h"
#include "Kismet/GameplayStatics.h"
#include "DialogueFunctionLibrary.h"
#include "../JivanaGameInstance.h"

//#define JIV_DEBUG_DIALOGUE_NODE

#ifdef JIV_DEBUG_DIALOGUE_NODE
	#define JIVLOG(text, ...) UE_LOG(LogTemp, Log, text, __VA_ARGS__)
#else
	#define JIVLOG(...)
#endif // JIV_DEBUG

UDialogueDisplayNode* UDialogueDisplayNode::DisplayDialogue(const UObject* WorldContextObject, const FString& JsonText, UWrapBox* WrapBoxWidget, TSubclassOf<UUserWidget> LetterWidgetClass, TSubclassOf<UUserWidget> ImageWidgetClass, TSubclassOf<UUserWidget> BreakWidgetClass, const TMap<FString, FLinearColor>& ColorMapping, const TMap<FString, FDialogueEvent>& EventMapping)
{
	UDialogueDisplayNode* BlueprintNode = NewObject<UDialogueDisplayNode>();
	BlueprintNode->WorldContextObject = WorldContextObject;
	BlueprintNode->JsonText = JsonText;
	BlueprintNode->WrapBoxWidget = WrapBoxWidget;
	BlueprintNode->EntryNum = 0;
	BlueprintNode->LetterWidgetClass = LetterWidgetClass;
	BlueprintNode->ImageWidgetClass = ImageWidgetClass;
	BlueprintNode->BreakWidgetClass = BreakWidgetClass;
	BlueprintNode->ColorMapping = &ColorMapping;
	BlueprintNode->EventMapping = &EventMapping;
	return BlueprintNode;
}

void UDialogueDisplayNode::Activate()
{
	this->Parse();

	FTimerHandleContainerWrapper wrapper;
	wrapper.ptr = MakeShareable(new FTimerHandleContainer());
	wrapper.ptr->isJivValid = true;
	this->Wrapper = wrapper;

	OnHandleCreated.Broadcast(wrapper);

	this->GetNextEntry();
	//WorldContextObject->GetWorld()->GetTimerManager().SetTimer(TimerHandleContainer->handle, this, &UDialogueDisplayNode::GetNextEntry, 0.01f);
}

TSharedPtr<FJsonObject> UDialogueDisplayNode::parseJSON(const FString& jsonText)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(jsonText);

	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject) && JsonObject.IsValid())
	{
		return JsonObject;
	}
	return nullptr;
}

void UDialogueDisplayNode::Parse()
{
	TSharedPtr<FJsonObject> json = parseJSON(this->JsonText);
	if (!json)
		return;

	TArray<TSharedPtr<FJsonValue>> dataArray = json->GetArrayField("data");
	TSharedPtr<FJsonObject> defaults = json->GetObjectField("defaults");

	double defaultSpeed = 0.05;
	defaults->TryGetNumberField("speed", defaultSpeed);
	this->DefaultSpeed = (float) defaultSpeed;
	this->CurrentSpeed = this->DefaultSpeed;

	this->DataArray = dataArray;
}

void UDialogueDisplayNode::GetNextEntry()
{
	JIVLOG(TEXT("0"));

	if (this->EntryNum >= this->DataArray.Num())
	{
		UE_LOG(LogTemp, Log, TEXT("GetNextEntry"));
		Wrapper.ptr->isJivValid = false;
		Wrapper.ptr = nullptr;
		OnFinished.Broadcast();
		return;
	}

	if (!UDialogueFunctionLibrary::IsTimerHandleValid(nullptr, Wrapper))
	{
		return;
	}

	JIVLOG(TEXT("1"));

	TSharedPtr<FJsonObject> obj = this->DataArray[this->EntryNum]->AsObject();
	FString type = obj->GetStringField("t");
	bool hasToIgnoreDelay = false;

	JIVLOG(TEXT("2"));

	if (type == "t" || type == "c") // If type=text || type=color
	{
		JIVLOG(TEXT("3"));

		JIVLOG(TEXT("3.1"));
		UUserWidget* widget = CreateWidget<UUserWidget>(WorldContextObject->GetWorld(), LetterWidgetClass);
		JIVLOG(TEXT("3.2"));
		UPanelWidget* RootWidget = Cast<UPanelWidget>(widget->GetRootWidget());
		JIVLOG(TEXT("3.3"));

		FString textKey = "v";
		if (type == "c")
		{
			textKey = "c";
		}

		JIVLOG(TEXT("4"));

		int32 outChildIndex = INDEX_NONE;
		JIVLOG(TEXT("4.1: %p"), RootWidget);
		UWidget* uw = widget->WidgetTree->FindWidgetChild(RootWidget, "TextBlock", outChildIndex);
		JIVLOG(TEXT("4.2: %p"), uw);
		UTextBlock* TextBlock = Cast<UTextBlock>(uw);
		JIVLOG(TEXT("4.3: %p"), TextBlock);
		FString sf = obj->GetStringField(textKey);
		JIVLOG(TEXT("4.4"));
		FText ft = FText::FromString(sf);
		JIVLOG(TEXT("4.5"));
		TextBlock->SetText(ft);
		JIVLOG(TEXT("4.6"));
		WrapBoxWidget->AddChild(widget);

		JIVLOG(TEXT("5"));

		if (type == "c")
		{
			TArray<TSharedPtr<FJsonValue>> outArray;
			const auto* ptr = &outArray;
			FString outString;

			JIVLOG(TEXT("6"));
			if (obj->TryGetArrayField("v", ptr))
			{
				auto& arr = obj->GetArrayField("v");
				float r = (float)arr[0]->AsNumber();
				float g = (float)arr[1]->AsNumber();
				float b = (float)arr[2]->AsNumber();
				float a = (float)arr[3]->AsNumber();
				TextBlock->SetColorAndOpacity(FLinearColor(r, g, b, a));
			}
			else if (obj->TryGetStringField("v", outString))
			{
				JIVLOG(TEXT("7"));

				if (this->ColorMapping && this->ColorMapping->Contains(outString))
				{
					TextBlock->SetColorAndOpacity((*this->ColorMapping)[outString]);
				}
				else
				{
					// TODO: It is not a valid type => emit a warning
				}
			}
			else
			{
				// TODO: It is not a valid type => emit a warning
			}
		}
	}
	else if (type == "i") // If type=image
	{
		JIVLOG(TEXT("8"));

		UUserWidget* widget = CreateWidget<UUserWidget>(WorldContextObject->GetWorld(), ImageWidgetClass);
		UPanelWidget* RootWidget = Cast<UPanelWidget>(widget->GetRootWidget());

		JIVLOG(TEXT("9"));

		int32 outChildIndex = INDEX_NONE;
		URichTextBlock* RichTextBlock = Cast<URichTextBlock>(widget->WidgetTree->FindWidgetChild(RootWidget, "RichTextBlock", outChildIndex));
		FString imgText = "<img id=\"" + obj->GetStringField("v") + "\"></>";
		RichTextBlock->SetText(FText::FromString(imgText));
		WrapBoxWidget->AddChild(widget);

		JIVLOG(TEXT("10"));
	}
	else if (type == "e") // If type=event
	{
		JIVLOG(TEXT("11"));

		FString eventName = obj->GetStringField("v");
		if (this->EventMapping && this->EventMapping->Contains(eventName))
		{
			JIVLOG(TEXT("12"));

			(*this->EventMapping)[eventName].Delegate.ExecuteIfBound();
		}
		else
		{
			// TODO: It is not a valid type => emit a warning
		}
	}
	else if (type == "b") // If type=break
	{
		JIVLOG(TEXT("13"));

		UUserWidget* widget = CreateWidget<UUserWidget>(WorldContextObject->GetWorld(), BreakWidgetClass);
		auto* panelSlot = WrapBoxWidget->AddChild(widget);
		auto* slot = Cast<UWrapBoxSlot>(panelSlot);
		slot->SetFillEmptySpace(true);
		slot->SetFillSpanWhenLessThan(10000.0f);

		JIVLOG(TEXT("14"));
	}
	else if (type == "s") // If type=style
	{
		JIVLOG(TEXT("15"));

		UUserWidget* widget = CreateWidget<UUserWidget>(WorldContextObject->GetWorld(), ImageWidgetClass);
		UPanelWidget* RootWidget = Cast<UPanelWidget>(widget->GetRootWidget());

		JIVLOG(TEXT("16"));

		int32 outChildIndex = INDEX_NONE;
		URichTextBlock* RichTextBlock = Cast<URichTextBlock>(widget->WidgetTree->FindWidgetChild(RootWidget, "RichTextBlock", outChildIndex));
		FString styleText = "<" + obj->GetStringField("v") + ">" + obj->GetStringField("c") + "</>";
		RichTextBlock->SetText(FText::FromString(styleText));
		WrapBoxWidget->AddChild(widget);

		JIVLOG(TEXT("17"));
	}
	else if (type == "sp") // If type=speed
	{
		JIVLOG(TEXT("18"));

		double speed = obj->GetNumberField("v");
		this->CurrentSpeed = (float) speed;
		hasToIgnoreDelay = true;

		JIVLOG(TEXT("19"));
	}
	else if (type == "rs") // If type=reset-speed
	{
		JIVLOG(TEXT("20"));

		this->CurrentSpeed = DefaultSpeed;
		hasToIgnoreDelay = true;

		JIVLOG(TEXT("21"));
	}
	else
	{
		// TODO: It is not a valid type => emit a warning
	}

	this->EntryNum++;

	if (!hasToIgnoreDelay && (this->CurrentSpeed > 0.0f))
	{
		//FTimerHandle handle;
		/*if (UJivanaGameInstance* GI = Cast<UJivanaGameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject->GetWorld())))
		{
			WorldContextObject->GetWorld()->GetTimerManager().SetTimer(GI->dialogueHandle.handle, this, &UDialogueDisplayNode::GetNextEntry, this->CurrentSpeed);
		}*/
		//UE_LOG(LogTemp, Log, TEXT("handle %s validity? %i"), *TimerHandleContainer->handle.ToString(), TimerHandleContainer->handle.IsValid());

		// TODO: UE_LOG(LogTemp, Log, TEXT("handle on check=%p"), &TimerHandleContainer);
		// TODO: if (TimerHandleContainer->isJivValid)
		if (UDialogueFunctionLibrary::IsTimerHandleValid(nullptr, Wrapper))
		{
			FTimerHandle handle;
			WorldContextObject->GetWorld()->GetTimerManager().SetTimer(handle, this, &UDialogueDisplayNode::GetNextEntry, this->CurrentSpeed);
		}
	}
	else
	{
		this->GetNextEntry();
	}
}

#undef JIVLOG
#undef JIV_DEBUG_DIALOGUE_NODE
