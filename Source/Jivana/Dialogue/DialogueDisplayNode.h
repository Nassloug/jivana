// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Jivana.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "Serialization/JsonReader.h"
#include "Dom/JsonObject.h"
#include "DialogueFunctionLibrary.h"
#include "DialogueDisplayNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDialogueDisplayEndOutputPin);

class UWrapBox;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDelayOneFrameOutputPin, FTimerHandleContainerWrapper, wrapper);

/**
 * 
 */
UCLASS()
class JIVANA_API UDialogueDisplayNode : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintAssignable)
		FDelayOneFrameOutputPin OnHandleCreated;

	UPROPERTY(BlueprintAssignable)
		FDialogueDisplayEndOutputPin OnFinished;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "Dialogue")
		static UDialogueDisplayNode* DisplayDialogue(const UObject* WorldContextObject, const FString& JsonText, UWrapBox* WrapBoxWidget, TSubclassOf<UUserWidget> LetterWidgetClass, TSubclassOf<UUserWidget> ImageWidgetClass, TSubclassOf<UUserWidget> BreakWidgetClass, const TMap<FString, FLinearColor>& ColorMapping, const TMap<FString, FDialogueEvent>& EventMapping);

	virtual void Activate() override;
private:
	TSharedPtr<FJsonObject> parseJSON(const FString& jsonText);

	void Parse();

	void GetNextEntry();
private:
	const UObject* WorldContextObject;
	FString JsonText;
	TArray<TSharedPtr<FJsonValue>> DataArray;
	float DefaultSpeed;
	float CurrentSpeed;
	int EntryNum;
	UWrapBox* WrapBoxWidget;
	TSubclassOf<UUserWidget> LetterWidgetClass;
	TSubclassOf<UUserWidget> ImageWidgetClass;
	TSubclassOf<UUserWidget> BreakWidgetClass;
	const TMap<FString, FLinearColor>* ColorMapping;
	const TMap<FString, FDialogueEvent>* EventMapping;
	FTimerHandleContainerWrapper Wrapper;
};
