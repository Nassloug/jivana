// Fill out your copyright notice in the Description page of Project Settings.

#include "PNJConversation.h"
#include "Components/SphereComponent.h"
#include "Components/PrimitiveComponent.h"
#include "../Controller/PlayerBase.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/DataTable.h"
#include "../DataTables/PNJDialogueRow.h"
#include "../Controller/ContextualCamera.h"
#include "../JivanaGameInstance.h"

// Sets default values
APNJConversation::APNJConversation()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->DialogueRowIndex = 0;

	this->AutoPossessAI = EAutoPossessAI::Disabled;
	this->AutoPossessPlayer = EAutoReceiveInput::Type::Disabled;
	this->AutoReceiveInput = EAutoReceiveInput::Type::Disabled;
	this->AIControllerClass = nullptr;

	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = SphereComponent;

#ifdef JIV_CAMERA_TEST
	DialogueCameraChild = CreateDefaultSubobject<UChildActorComponent>(TEXT("DialogueCamera"));
	DialogueCameraChild->SetChildActorClass(AContextualCamera::StaticClass());
#else
	DialogueCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("DialogueCamera"));
#endif

	this->IsDialogueStarted = false;

	this->DialogueTriggerRange = SphereComponent;
	this->DialogueTriggerRange->OnComponentBeginOverlap.AddDynamic(this, &APNJConversation::OnTriggerOverlapBegin);
	this->DialogueTriggerRange->OnComponentEndOverlap.AddDynamic(this, &APNJConversation::OnTriggerEndOverlap);
}

// Called when the game starts or when spawned
void APNJConversation::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APNJConversation::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APNJConversation::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("LaunchDialogue", IE_Pressed, this, &APNJConversation::PassDialogue);
}

FPNJDialogueRow* GetDialogueAtIndex(const UDataTable* DataTable, int Index)
{
	TArray<FName> names = DataTable->GetRowNames();
	if (!names.IsValidIndex(Index))
	{
		UE_LOG(LogTemp, Log, TEXT("Dialogue row index is out of bounds: %i"), Index);
		return nullptr;
	}

	FName& rownName = names[Index];
	FPNJDialogueRow* dialogue = DataTable->FindRow<FPNJDialogueRow>(rownName, TEXT("Dialogue"), true);
	return dialogue;
}

void APNJConversation::PossessedBy(AController* NewController)
{
	if (!NewController)
		return;

	if (IsDialogueStarted)
		return;

	if (!DataTable)
		return;

	auto& cameraTransform = this->Player->cameraPlayer->GetComponentTransform();
#ifdef JIV_CAMERA_TEST
	this->DialogueCameraChild->SetWorldTransform(cameraTransform);

	this->DialogueCamera = Cast<AContextualCamera>(DialogueCameraChild->GetChildActor());
	this->Player->UseContextualCamera(this->DialogueCamera, NewController);
#else
	this->DialogueCamera->SetWorldTransform(cameraTransform);
#endif

	FPNJDialogueRow* dialogue = GetDialogueAtIndex(DataTable, this->DialogueRowIndex);

	this->DisplayDialogue(dialogue);

	IsDialogueStarted = true;
}

void APNJConversation::DisplayDialogue(FPNJDialogueRow* const dialogue)
{
	if (dialogue == nullptr)
		return;

	if (!this->Player)
		return;

	this->Player->StartDialogueInOverlay(*dialogue);
}

void APNJConversation::OnEndDialogue()
{
	if (!this->IsDialogueStarted)
		return;

	this->IsDialogueStarted = false;
	this->DialogueRowIndex = 0;

	UE_LOG(LogTemp, Log, TEXT("DIALOGUE FINISHED"));

	this->Player->StopDialogueInOverlay();

	/*if (UJivanaGameInstance* GI = Cast<UJivanaGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())))
	{
		// TODO: stop dialogue handle
		if (GI->dialogueHandle.handle.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(GI->dialogueHandle.handle);
		}
	}*/

	// TODO: remove UI

	this->Player->UsePlayerCamera(false);

	if (APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0))
	{
		PC->Possess(this->Player);
	}
}

void APNJConversation::PassDialogue()
{
	if (!this->IsDialogueStarted || !this->Player)
		return;

	const int32 maxDialogueCount = DataTable->GetRowNames().Num();
	this->DialogueRowIndex++;

	if (this->DialogueRowIndex >= maxDialogueCount)
	{
		this->OnEndDialogue();
		return;
	}

	UE_LOG(LogTemp, Log, TEXT("Go to next dialogue: %i/%i"), this->DialogueRowIndex, maxDialogueCount);

	FPNJDialogueRow* dialogue = GetDialogueAtIndex(DataTable, this->DialogueRowIndex);

	this->DisplayDialogue(dialogue);
}

void APNJConversation::OnTriggerOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		APlayerBase* player = Cast<APlayerBase>(OtherActor);

		if (!player)
			return;

		player->PNJConversation = this;
		this->Player = player;

		if (CallPlayer)
		{
			if (APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0))
			{
				PC->Possess(this);
			}
		}
	}
}

void APNJConversation::OnTriggerEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		APlayerBase* player = Cast<APlayerBase>(OtherActor);

		if (!player)
			return;

		this->OnEndDialogue();

		player->PNJConversation = nullptr;
		this->Player = nullptr;
	}
}
