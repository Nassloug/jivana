// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Jivana.h"
#include "ContextualCamera.h"
#include "../LevelDesign/Plant.h"
#include "../LevelDesign/InteractibleObject.h"
#include "../LevelDesign/Fruit.h"
#include "NiagaraComponent.h"
#include "Engine/DataTable.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SplineComponent.h"
#include "Camera/CameraActor.h"
#include "Camera/CameraComponent.h"
#include "Containers/Array.h"
#include "Curves/CurveFloat.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/StaticMeshComponent.h"
#include "../DataTables/PNJDialogueRow.h"
#include "PlayerBase.generated.h"

UCLASS()
class JIVANA_API APlayerBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// --- Controller Functions --- //
	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Read controller data table and initialize parameters
	UFUNCTION(BlueprintCallable, Category = "Controller")
		void InitializeControllerType(FString type);

	// Set camera check collision
	UFUNCTION(BlueprintCallable, Category = "Controller")
		void SetCameraCheckCollision(bool bCheckCollision);

	// --- Environment Functions --- //

	// Cast rays to detect ledge
	UFUNCTION(BlueprintNativeEvent, Category = "Environment")
		EObstacle CheckLedge();

	// --- Camera Functions --- //

	// Set camera behind player
	UFUNCTION(BlueprintCallable, Category = "Camera")
		void ResetCamera();

	UFUNCTION(BlueprintCallable, Category = "Camera")
		void UseContextualCamera(AContextualCamera* cam, float blendTime, AController* targetController = nullptr);

	UFUNCTION(BlueprintCallable, Category = "Camera")
		void UsePlayerCamera(bool tpsCam);

	// Check if actor is in line of sight
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Camera")
		bool CheckCameraBlocking(AActor* actor);

	UFUNCTION(BlueprintNativeEvent, Category = "Camera")
		APlant* CheckTarget();

	// Check if component is in line of sight
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Camera")
		bool CheckCameraBlockingPosition(AActor* actor, FVector position);

	// --- Attack Functions --- //

	// Apply damage on possible targets
	UFUNCTION(BlueprintNativeEvent, Category = "Attack")
		void CheckDamageHit(uint8 damage);

	// Draw attack debug box
	UFUNCTION(BlueprintNativeEvent, Category = "Attack")
		void DrawAttackDebugBox();

	// --- Water Absorption --- //

	// Try to absorbe water and return true in case of success
	UFUNCTION(BlueprintNativeEvent, Category = "Water Absorption")
		EFlowLevel TryWaterAbsorption();

	// Stun current target
	UFUNCTION(BlueprintNativeEvent, Category = "Water Absorption")
		void StunTarget();

	UPROPERTY(Category = "Camera", BlueprintReadWrite)
		AContextualCamera* contextualCamera;
	
	// --- Camera Properties --- //

	UPROPERTY(Category = "Camera", BlueprintReadWrite)
		UCameraComponent* cameraPlayer;

	// --- Input Storage Properties --- //

	UPROPERTY(Category = "Inputs", BlueprintReadWrite)
		float rightInput;

	UPROPERTY(Category = "Inputs", BlueprintReadWrite)
		float forwardInput;

	UPROPERTY(Category = "Inputs", BlueprintReadWrite)
		float cameraRightInput;

	UPROPERTY(Category = "Inputs", BlueprintReadWrite)
		float cameraLookUpInput;

	// --- State Control Booleans --- //

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsJumping;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bHasTargetLocked;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bHasTriggeredAttack;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsGrabbingLedge;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsAboutToFall;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsClimbing;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsJumpingOverObstacle;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsLeaping;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsAbsorbing;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsReleasing;

	UPROPERTY(Category = "State", BlueprintReadWrite)
		bool bIsDead;

	// --- Dialogue --- //

	UFUNCTION(BlueprintNativeEvent, Category = "Dialogue")
		void StartDialogueInOverlay(const FPNJDialogueRow& dialogue);

	UFUNCTION(BlueprintNativeEvent, Category = "Dialogue")
		void StopDialogueInOverlay();

	UPROPERTY(Category = "Dialogue", BlueprintReadOnly)
		class APNJConversation* PNJConversation;

private:

	// --- Common Variables --- //

	FTimerHandle cameraLagHandle;

	UCharacterMovementComponent* characterMovement;
	TArray<AActor*> inRangePlants;
	TArray<AActor*> inRangeFruits;
	APlant* focusedPlant;

	EFlowLevel flow;

	FVector2D viewportSize;

	float lastDeltaTime;

	FRotator initialSpringRotation;
	FRotator initialCameraRotation;

	FVector jumpMomentum;
	FVector jumpStartPosition;

	float moveSpeedMultiplier;
	float rotationSpeedMultiplier;

	float jumpingTimer;
	float splineTimer;
	float lateJumpTimer;
	float ledgeTimer;
	float cameraTurningTimer;
	float autoFollowingTimer;

	bool bIsFollowingTriggered;

	// Temporary direction for focused plant

	float focusedDirectionX;
	float focusedDirectionY;

	// --- Camera Properties --- //

	float cameraSpeed;
	float cameraDistance;
	FVector initialCameraAzimuth;
	FVector cameraSpringPosition;
	FVector cameraPositionOffset;
	FRotator cameraRotation;
	float cameraLag;
	float cameraRotationLag;
	float mouseSensitivity;
	float cameraFOV;

	float cameraMaxAngleY;
	float cameraMinAngleY;

	bool characterFollowAiming;

	float aimingCameraMaxAngleY;
	float aimingCameraMinAngleY;

	bool bIsTurningOnMovement;
	float turningSpeed;
	float turningDelay;

	bool bIsAutoFollowing;
	float autoFollowingSpeed;
	float autoFollowingDelay;
	float autoFollowingMinRange;

	bool bIsEnableCameraFlow;
	FVector cameraFlowSpeed;
	FVector cameraFlowRecoverySpeed;
	FVector cameraFlowAmplitude;

	// --- Target Tracking Properties --- //

	float maxTrackingDistance;
	float trackingLag;
	float trackingRotationLag;

	// --- Character Properties --- //

	float characterMaxSpeed;
	float characterAcceleration;
	float characterHeight;

	bool bCanJump;
	bool bIsJumpFollowingCurve;
	bool bIsJumpFollowingGravity;
	bool bIsDisableRotationOnJump;

	UCurveFloat* jumpCurve;
	UCurveFloat* gravityCurve;

	float gravityCurveMagnitude;
	float gravityScale;
	float jumpForce;
	float jumpForwardImpulse;
	float jumpCurveMagnitude;
	float fixedJumpDistance;
	float fixedJumpThreshold;
	float airControl;

	FVector fixedJumpDirection;

	// --- Ledge Behaviour Properties --- //

	bool bAutoClimb;

	float leapStandardDistance;
	float leapDuration;
	float jumpOverObstacleDuration;
	float climbDuration;

	FVector2D leapTengent;
	FVector2D jumpOverObstacleTengent;
	FVector2D climbTengent;

	// --- Attack Properties --- //

	float attackTime;
	float recoveryTime;

	// --- Level Properties --- //

	AActor* currentTarget;
	float targetAnchorDistance;

	// -------------------- Functions -------------------- //

	// --- Camera Functions --- //

	// Make camera follow current target
	void CameraFollowTarget();

	// Smoothly place camera behind player
	void CameraAutoFollow(float DeltaTime);

	// --- Events Processing --- //

	// Process jump execution
	void ProcessJump();

	// Process player behaviour on a ledge
	void ProcessLedge();

	// --- State Control --- //

	// --- Utility Functions --- //

	// Rotate player mesh to face locked target
	void FaceTarget();

	// Move player along contextual spline
	void FollowSpline(float deltaTime);

	// Detect plants in range of player
	void DetectPlants();

	// Focus camera on target
	void TriggerTarget(AActor* target);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintNativeEvent, Category = "Water")
		void WaterBend(AInteractibleObject* TargetInteractible);

	// --- Water Properties --- //

	UPROPERTY(Category = "Water", BlueprintReadWrite)
		UNiagaraComponent* waterbendEffect;

	UPROPERTY(Category = "Water", BlueprintReadWrite)
		FVector aimingOrientation;

	UPROPERTY(Category = "Water", BlueprintReadWrite)
		FVector waterCastHitPosition;

	UPROPERTY(Category = "Water", BlueprintReadWrite)
		FVector aimingSnapLocation;

	// --- Camera Properties --- //

	UPROPERTY(Category = "Camera", BlueprintReadWrite)
		USceneComponent* cameraAzimuth;

	UPROPERTY(Category = "Camera", BlueprintReadWrite)
		USpringArmComponent* cameraSpring;

	// --- Character Properties --- //

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UDataTable* controllerParameters;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite)
		FString controllerType;

	UPROPERTY(Category = "Character", BlueprintReadWrite)
		USplineComponent* contextualSpline;


	// --- Ledge Behaviour Properties --- //

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector2D minDetectionOffset;

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector minDetectionExtents;

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector2D midDetectionOffset;

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector midDetectionExtents;

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector2D maxDetectionOffset;

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector maxDetectionExtents;

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector2D autoJumpDetectionOffset;

	UPROPERTY(Category = "Ledge", BlueprintReadOnly)
		FVector autoJumpDetectionExtents;

	UPROPERTY(Category = "Ledge", BlueprintReadWrite)
		FVector ledgeDirection;

	// --- Axis Binds --- //

	// Process forward/back input
	void MoveForward(float value);

	// Process right/left input
	void MoveRight(float value);

	// Process camera right/left controller input
	void CameraTurn(float value);

	// Process camera up/down controller input
	void CameraLookUp(float value);

	// Process camera right/left mouse input
	void CameraTurnRate(float value);

	// Process camera up/down mouse input
	void CameraLookUpRate(float value);

	// --- Action Binds --- //

	// Process target lock input
	void TargetLockPressed();

	// Process jump input
	void JumpPressed();

	// Process jump release
	void JumpReleased();

	// Process absorb input
	void AbsorbPressed();

	// Process absorb release
	void AbsorbReleased();

	// Process release input
	void ReleasePressed();

	// Process release release
	void ReleaseReleased();

	// Process launch dialogue input
	void LaunchDialoguePressed();

	// --- Delay Functions --- //

	// Delay disable camera lag
	void DelayDisableCameraLag();

};