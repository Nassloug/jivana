// Fill out your copyright notice in the Description page of Project Settings.


#include "ContextualCamera.h"
#include "../DataTables/ContextualCameraRow.h"

void AContextualCamera::InitializeContextualCameraType()
{
	FString ContextString(TEXT("Camera Parameters"));
	FContextualCameraRow* cameraData = cameraParameters->FindRow<FContextualCameraRow>(FName(cameraType), ContextString, true);

	if (cameraData != NULL)
	{
		bIsControlledBySequence = cameraData->bIsControlledBySequence;

		constraintPositionX = cameraData->constraintPositionX;
		constraintPositionY = cameraData->constraintPositionY;
		constraintPositionZ = cameraData->constraintPositionZ;

		positionOffset = cameraData->positionOffset;

		constraintRotationPitch = cameraData->constraintRotationPitch;
		constraintRotationYaw = cameraData->constraintRotationYaw;
		constraintRotationRoll = cameraData->constraintRotationRoll;

		followTargetWithPosition = cameraData->followTargetWithPosition;
		followTargetWithRotation = cameraData->followTargetWithRotation;
		followTrail = cameraData->followTrail;
	}
}

void AContextualCamera::UpdateCamera()
{
	if (bIsControlledBySequence)
	{
		return;
	}
	if (target != NULL)
	{

		if (followTargetWithPosition)
		{
			float newX = GetActorLocation().X;
			float newY = GetActorLocation().Y;
			float newZ = GetActorLocation().Z;

			if (!constraintPositionX)
			{
				newX = target->GetActorLocation().X + positionOffset.X;
			}
			if (!constraintPositionY)
			{
				newY = target->GetActorLocation().Y + positionOffset.Y;
			}
			if (!constraintPositionZ)
			{
				newZ = target->GetActorLocation().Z + positionOffset.Z;
			}

			SetActorLocation(FVector(newX, newY, newZ));
		}
		else if (followTrail && cameraTrail != NULL)
		{
			SetActorLocation(cameraTrail->FindLocationClosestToWorldLocation(target->GetActorLocation() + positionOffset, ESplineCoordinateSpace::World));
		}

		if (followTargetWithRotation)
		{
			FRotator targetRotation = (target->GetActorLocation() - GetActorLocation()).Rotation();

			if (constraintRotationPitch)
			{
				targetRotation = FRotator(GetActorRotation().Pitch, targetRotation.Yaw, targetRotation.Roll);
			}

			if (constraintRotationYaw)
			{
				targetRotation = FRotator(targetRotation.Pitch, GetActorRotation().Yaw, targetRotation.Roll);
			}

			if (constraintRotationRoll)
			{
				targetRotation = FRotator(targetRotation.Pitch, targetRotation.Yaw, GetActorRotation().Roll);
			}

			SetActorRotation(targetRotation);
		}

	}

}