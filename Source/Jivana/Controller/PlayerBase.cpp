// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerBase.h"
#include "TimerManager.h"
#include "../DataTables/ControllerRow.h"
#include "GameFramework/WorldSettings.h"
#include "GameFramework/Controller.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "../Dialogue/PNJConversation.h"
#include "../LevelDesign/InteractibleObject.h"

// Sets default values
APlayerBase::APlayerBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void APlayerBase::BeginPlay()
{
	Super::BeginPlay();

	autoFollowingTimer = 0;
	cameraTurningTimer = 0;

	moveSpeedMultiplier = 1;
	rotationSpeedMultiplier = 1;

	flow = EFlowLevel::NONE;

	focusedPlant = NULL;

	characterMovement = GetCharacterMovement();

	if (cameraPlayer != NULL)
	{
		initialCameraRotation = cameraPlayer->GetRelativeRotation();
	}

	if (cameraSpring != NULL)
	{
		initialSpringRotation = cameraSpring->GetRelativeRotation();
	}

	InitializeControllerType(controllerType);

	initialCameraAzimuth = cameraAzimuth->GetRelativeLocation();
	cameraFOV = cameraPlayer->FieldOfView;
}

// Called every frame
void APlayerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	lastDeltaTime = DeltaTime;

	rightInput = 0;
	forwardInput = 0;
	cameraRightInput = 0;	
	cameraLookUpInput = 0;

	CameraFollowTarget();
	DetectPlants();

	ProcessLedge();

	if (contextualCamera != NULL)
	{
		contextualCamera->UpdateCamera();
	}

	if (characterMovement->MovementMode == EMovementMode::MOVE_Falling)
	{
		lateJumpTimer += DeltaTime;
	}
	else 
	{
		lateJumpTimer = 0;
	}

	if (characterMovement->MovementMode == EMovementMode::MOVE_Walking && bIsDisableRotationOnJump)
	{
		characterMovement->bOrientRotationToMovement = true;
	}

	if (!bIsReleasing && cameraPlayer->FieldOfView < cameraFOV)
	{
		cameraPlayer->FieldOfView = UKismetMathLibrary::FMin(cameraFOV, cameraPlayer->FieldOfView + DeltaTime * 100.0f);
	}

	if (bIsAbsorbing || bIsReleasing)
	{
		if (characterFollowAiming)
		{
			FRotator targetRotation = ((aimingOrientation - GetActorLocation()) * FVector(1, 1, 0)).GetSafeNormal().Rotation();
			SetActorRotation(targetRotation);
		}

		if (bIsAbsorbing)
		{
			FVector azimuthOffset = (FVector(15, 100, 100) - initialCameraAzimuth) * DeltaTime * 7.5f;
			cameraAzimuth->AddRelativeLocation(azimuthOffset, true);

			if (cameraAzimuth->GetRelativeLocation().X >= 15)
			{
				cameraAzimuth->SetRelativeLocation(FVector(15, 100, 100));
			}
		}
		else
		{
			FVector azimuthOffset = (FVector(15, -100, 100) - initialCameraAzimuth) * DeltaTime * 7.5f;
			cameraAzimuth->AddRelativeLocation(azimuthOffset, true);

			if (cameraAzimuth->GetRelativeLocation().X >= 15)
			{
				cameraAzimuth->SetRelativeLocation(FVector(15, -100, 100));
			}
		}

		// Check plant
		if (focusedPlant != NULL)
		{
			if (bIsReleasing)
			{
				if (bIsAbsorbing)
				{
					if (cameraPlayer->FieldOfView > cameraFOV - 10.0f)
					{
						cameraPlayer->FieldOfView = UKismetMathLibrary::FMax(cameraFOV - 10.0f, cameraPlayer->FieldOfView - DeltaTime * 100.0f);
					}

					float focusedRate = UKismetMathLibrary::Abs(focusedDirectionX) + UKismetMathLibrary::Abs(focusedDirectionY);
					float directionRate = UKismetMathLibrary::Abs(focusedPlant->directionX) + UKismetMathLibrary::Abs(focusedPlant->directionY);
					if (focusedRate >= 0.8f)
					{
						focusedPlant->directionX = focusedDirectionX /= focusedRate;
						focusedPlant->directionY = focusedDirectionY /= focusedRate;
					}

					if (directionRate >= 0.5f && focusedRate <= 0.2f)
					{
						//focusedPlant->ProcessInput(this, true);
						WaterBend(focusedPlant);
						focusedPlant->StopTrigger();
						focusedPlant = NULL;
						bIsReleasing = false;
					}
				}
				else
				{
					focusedPlant->ProcessInput(this, false);
				}
			}
		}
	}	

	// Check absorbing slow motion
	/*
	if (bIsAbsorbing)
	{
		GetWorldSettings()->SetTimeDilation(UKismetMathLibrary::FMax(GetWorldSettings()->GetEffectiveTimeDilation() - DeltaTime * 4.0f, 0.2f));
		characterMovement->Velocity = FVector::ZeroVector;
	}
	else
	{
		GetWorldSettings()->SetTimeDilation(UKismetMathLibrary::FMin(GetWorldSettings()->GetEffectiveTimeDilation() + DeltaTime * 15.0f, 1));
	}
	*/

	if (bIsJumping)
	{
		jumpingTimer += DeltaTime;
		ProcessJump();
	}

	if (bIsAutoFollowing && !bHasTargetLocked)
	{
		autoFollowingTimer += DeltaTime;
		if (autoFollowingTimer >= autoFollowingDelay)
		{
			CameraAutoFollow(DeltaTime);
		}
	}

	focusedDirectionX = 0;
	focusedDirectionY = 0;
}

// --- Controller Functions --- //
#pragma region Controller Functions

// Called to bind functionality to input
void APlayerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Bind axis
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerBase::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &APlayerBase::CameraTurn);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerBase::CameraLookUp);
	PlayerInputComponent->BindAxis("TurnRate", this, &APlayerBase::CameraTurnRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APlayerBase::CameraLookUpRate);

	// Bind actions
	PlayerInputComponent->BindAction("Target", IE_Pressed, this, &APlayerBase::TargetLockPressed);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlayerBase::JumpPressed);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &APlayerBase::JumpReleased);
	PlayerInputComponent->BindAction("Absorb", IE_Pressed, this, &APlayerBase::AbsorbPressed);
	PlayerInputComponent->BindAction("Absorb", IE_Released, this, &APlayerBase::AbsorbReleased);
	PlayerInputComponent->BindAction("Release", IE_Pressed, this, &APlayerBase::ReleasePressed);
	PlayerInputComponent->BindAction("Release", IE_Released, this, &APlayerBase::ReleaseReleased);
	PlayerInputComponent->BindAction("LaunchDialogue", IE_Pressed, this, &APlayerBase::LaunchDialoguePressed);

	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

// Read controller data table and initialize parameters
void APlayerBase::InitializeControllerType(FString type)
{
	FString ContextString(TEXT("Controller Parameters"));
	FControllerRow* controllerData = controllerParameters->FindRow<FControllerRow>(FName(type), ContextString, true);

	if (controllerData != NULL)
	{
		// Camera //
		cameraDistance = controllerData->cameraDistance;
		cameraSpeed = controllerData->cameraSpeed;
		cameraSpringPosition = controllerData->cameraSpringPosition;
		cameraPositionOffset = controllerData->cameraPositionOffset;
		cameraRotation = controllerData->cameraRotation;
		cameraLag = controllerData->cameraLag;
		cameraRotationLag = controllerData->cameraRotationLag;
		mouseSensitivity = controllerData->mouseSensitivity;

		cameraMaxAngleY = controllerData->cameraMaxAngleY;
		cameraMinAngleY = controllerData->cameraMinAngleY;

		characterFollowAiming =  controllerData->characterFollowAiming;

		aimingCameraMaxAngleY = controllerData->aimingCameraMaxAngleY;
		aimingCameraMinAngleY = controllerData->aimingCameraMinAngleY;

		bIsTurningOnMovement = controllerData->bIsTurningOnMovement;
		turningSpeed = controllerData->turningSpeed;
		turningDelay = controllerData->turningDelay;;
		bIsAutoFollowing = controllerData->bIsAutoFollowing;
		autoFollowingSpeed = controllerData->autoFollowingSpeed;
		autoFollowingDelay = controllerData->autoFollowingDelay;
		autoFollowingMinRange = controllerData->autoFollowingMinRange;

		bIsEnableCameraFlow = controllerData->bIsEnableCameraFlow;
		cameraFlowSpeed = controllerData->cameraFlowSpeed;
		cameraFlowRecoverySpeed = controllerData->cameraFlowRecoverySpeed;
		cameraFlowAmplitude = controllerData->cameraFlowAmplitude;

		// Target Tracking //
		maxTrackingDistance = controllerData->maxTrackingDistance;
		trackingLag = controllerData->trackingLag;
		trackingRotationLag = controllerData->trackingRotationLag;

		// Level //
		targetAnchorDistance = controllerData->targetAnchorDistance;

		// Character //
		characterMaxSpeed = controllerData->characterMaxSpeed;
		characterAcceleration = controllerData->characterAcceleration;
		characterHeight = controllerData->characterHeight;
		jumpCurve = controllerData->jumpCurve;
		gravityCurve = controllerData->gravityCurve;
		bCanJump = controllerData->bCanJump;
		jumpForce = controllerData->jumpForce;
		jumpForwardImpulse =  controllerData->jumpForwardImpulse;
		jumpCurveMagnitude = controllerData->jumpCurveMagnitude;
		airControl = controllerData->airControl;
		fixedJumpDistance = controllerData->fixedJumpDistance;
		fixedJumpThreshold = controllerData->fixedJumpThreshold;
		bIsJumpFollowingCurve = controllerData->bIsJumpFollowingCurve;
		bIsJumpFollowingGravity = controllerData->bIsJumpFollowingGravity;
		bIsDisableRotationOnJump = controllerData->bIsDisableRotationOnJump;
		gravityScale = controllerData->gravityScale;
		gravityCurveMagnitude = controllerData->gravityCurveMagnitude;

		// Ledge Behaviour //

		leapStandardDistance = controllerData->leapStandardDistance;
		leapDuration = controllerData->leapDuration;
		jumpOverObstacleDuration = controllerData->jumpOverObstacleDuration;
		climbDuration = controllerData->climbDuration;

		bAutoClimb = controllerData->bAutoClimb;

		autoJumpDetectionOffset = controllerData->autoJumpDetectionOffset;
		autoJumpDetectionExtents = controllerData->autoJumpDetectionExtents;

		leapTengent = controllerData->leapTengent;
		jumpOverObstacleTengent = controllerData->jumpOverObstacleTengent;
		climbTengent = controllerData->climbTengent;

		minDetectionOffset = controllerData->minDetectionOffset;
		minDetectionExtents = controllerData->minDetectionExtents;

		midDetectionOffset = controllerData->midDetectionOffset;
		midDetectionExtents = controllerData->midDetectionExtents;

		maxDetectionOffset = controllerData->maxDetectionOffset;
		maxDetectionExtents = controllerData->maxDetectionExtents;
	}

	if (characterMovement != NULL)
	{
		characterMovement->MaxWalkSpeed = characterMaxSpeed;
		characterMovement->JumpZVelocity = jumpForce;
		characterMovement->AirControl = airControl;
		characterMovement->MaxAcceleration = characterAcceleration;
		characterMovement->GravityScale = gravityScale;
	}

	if (cameraSpring != NULL)
	{
		cameraSpring->CameraLagSpeed = cameraLag;
		cameraSpring->CameraRotationLagSpeed = cameraRotationLag;
		cameraSpring->TargetArmLength = cameraDistance;
		cameraSpring->SetRelativeLocation(cameraSpringPosition);
		cameraSpring->TargetOffset = cameraPositionOffset;
	}

	if (cameraPlayer != NULL)
	{
		initialCameraRotation = cameraRotation;
		cameraPlayer->SetRelativeRotation(cameraRotation);
	}

	ResetCamera();
}

void APlayerBase::SetCameraCheckCollision(bool bCheckCollision)
{
	if (cameraSpring != NULL)
	{
		cameraSpring->bDoCollisionTest = bCheckCollision;
	}
}

#pragma endregion Controller Functions

// --- Environment Functions --- //
#pragma region Environment Functions

// Cast rays to detect ledge
EObstacle APlayerBase::CheckLedge_Implementation()
{
	return EObstacle::NONE;
}

void APlayerBase::WaterBend_Implementation(AInteractibleObject* InteractibleTarget)
{

}

#pragma endregion Environment Functions

// --- Camera Functions --- //
#pragma region Camera Functions

void APlayerBase::UseContextualCamera(AContextualCamera* cam, float blendTime, AController* targetController)
{
	if (contextualCamera != NULL)
	{
		contextualCamera->GetCameraComponent()->Deactivate();
	}
	if (targetController != NULL)
	{
		cam->GetCameraComponent()->Activate();
		targetController->CastToPlayerController()->SetViewTargetWithBlend(contextualCamera, blendTime);
	}
	else
	{
		contextualCamera = cam;
		contextualCamera->GetCameraComponent()->Activate();
		Controller->CastToPlayerController()->SetViewTargetWithBlend(contextualCamera, blendTime);
	}
}

void APlayerBase::UsePlayerCamera(bool tpsCam)
{
	if (contextualCamera != NULL)
	{
		contextualCamera->GetCameraComponent()->Deactivate();
	}
	contextualCamera = NULL;
	if (tpsCam)
	{
		if (characterFollowAiming)
		{
			characterMovement->RotationRate = FRotator(0, 0, 0);
		}

		// Trigger DelayDisableCameraLag after 0.1s
		GetWorldTimerManager().SetTimer(cameraLagHandle, this, &APlayerBase::DelayDisableCameraLag, 1, false);

		cameraSpring->bEnableCameraLag = false;
		cameraSpring->bEnableCameraRotationLag = false;

	}
	else
	{

		characterMovement->RotationRate = FRotator(0, 720, 0);
		cameraAzimuth->SetRelativeLocation(initialCameraAzimuth, true);

		cameraSpring->bEnableCameraLag = true;
		cameraSpring->bEnableCameraRotationLag = true;

	}
}

APlant* APlayerBase::CheckTarget_Implementation()
{
	return NULL;
}

// Check if actor is in line of sight
bool APlayerBase::CheckCameraBlocking_Implementation(AActor* actor) 
{
	return false;
}

// Check if component is in line of sight
bool APlayerBase::CheckCameraBlockingPosition_Implementation(AActor* actor, FVector position) 
{
	return false;
}

#pragma endregion Camera Functions 

// --- Attack Functions --- //
#pragma region Debug Functions

// Apply damage on possible targets
void APlayerBase::CheckDamageHit_Implementation(uint8 damage) {}

// Draw attack debug box
void APlayerBase::DrawAttackDebugBox_Implementation() {}

#pragma endregion Debug Functions 

// --- Water Absorption --- //
#pragma region Water Absorption

// Try to absorb water and return true in case of success
EFlowLevel APlayerBase::TryWaterAbsorption_Implementation()
{
	return EFlowLevel::NONE;
}

// Stun current target
void APlayerBase::StunTarget_Implementation() {}

#pragma endregion Water Absorption

// --- Dialogue --- //
#pragma region Dialogue

void APlayerBase::StartDialogueInOverlay_Implementation(const FPNJDialogueRow& dialogue) {}

void APlayerBase::StopDialogueInOverlay_Implementation() {}

#pragma endregion Dialogue

// --- Camera Functions --- //
#pragma region Camera Functions

// Set camera behind player
void APlayerBase::ResetCamera()
{
	if (cameraSpring != NULL)
	{
		cameraSpring->SetRelativeRotation(GetMesh()->GetComponentRotation());
	}
}

// Make camera follow current target
void APlayerBase::CameraFollowTarget()
{
	if (bHasTargetLocked && currentTarget != NULL)
	{
		if (FVector::Distance(currentTarget->GetActorLocation(), GetActorLocation()) > maxTrackingDistance)
		{
			bHasTargetLocked = false;
			currentTarget = NULL;
		}
		else
		{
			cameraSpring->CameraLagSpeed = trackingLag;
			cameraSpring->CameraRotationLagSpeed = trackingRotationLag;

			FVector target = (currentTarget->GetActorLocation() - GetActorLocation() - FVector(0, 0, 90)) / 2;

			cameraSpring->SetRelativeRotation(target.Rotation());
		}
	}
	else
	{
		cameraSpring->CameraLagSpeed = cameraLag;
		cameraSpring->CameraRotationLagSpeed = cameraRotationLag;
	}
}

// Smoothly place camera behind player
void APlayerBase::CameraAutoFollow(float DeltaTime)
{
	if (cameraSpring != NULL)
	{
		if (UKismetMathLibrary::Abs(FVector::DotProduct(cameraSpring->GetForwardVector(), GetActorForwardVector()) - 1) >= autoFollowingMinRange || bIsFollowingTriggered)
		{
			bIsFollowingTriggered = true;
			cameraSpring->SetRelativeRotation(UKismetMathLibrary::RLerp(cameraSpring->GetRelativeRotation(), GetActorForwardVector().Rotation(), DeltaTime * autoFollowingSpeed / 10.0f, true));
		}
	}
}

#pragma endregion Camera Functions

// --- Axis Binds --- //
#pragma region Axis Binds

// Process forward/back input
void APlayerBase::MoveForward(float value)
{

	if (Controller != NULL)
	{
		forwardInput += value;
		if (!bIsDead && !bIsLeaping & !bIsClimbing && !bIsJumpingOverObstacle && !bIsGrabbingLedge)
		{
			if (value != 0 && !bIsAbsorbing)
			{
				float cameraYaw = initialCameraRotation.Yaw;
				if (cameraPlayer->GetRelativeRotation().Yaw > initialCameraRotation.Yaw)
				{
					cameraYaw = cameraPlayer->GetRelativeRotation().Yaw - lastDeltaTime * cameraFlowRecoverySpeed.Z;
					if (cameraYaw < initialCameraRotation.Yaw)
					{
						cameraYaw = cameraPlayer->GetRelativeRotation().Yaw + (initialCameraRotation.Yaw - cameraPlayer->GetRelativeRotation().Yaw) * lastDeltaTime;
					}
				}
				else if (cameraPlayer->GetRelativeRotation().Yaw < initialCameraRotation.Yaw)
				{
					cameraYaw = cameraPlayer->GetRelativeRotation().Yaw + lastDeltaTime * cameraFlowRecoverySpeed.Z;
					if (cameraYaw > initialCameraRotation.Yaw)
					{
						cameraYaw = cameraPlayer->GetRelativeRotation().Yaw + (initialCameraRotation.Yaw - cameraPlayer->GetRelativeRotation().Yaw) * lastDeltaTime;
					}
				}

				float cameraRoll = initialCameraRotation.Roll;
				if (cameraPlayer->GetRelativeRotation().Roll > initialCameraRotation.Roll)
				{
					cameraRoll = cameraPlayer->GetRelativeRotation().Roll - lastDeltaTime * cameraFlowRecoverySpeed.X;
					if (cameraRoll < initialCameraRotation.Roll)
					{
						cameraRoll = cameraPlayer->GetRelativeRotation().Roll + (initialCameraRotation.Roll - cameraPlayer->GetRelativeRotation().Roll) * lastDeltaTime;
					}
				}
				else if (cameraPlayer->GetRelativeRotation().Roll < initialCameraRotation.Roll)
				{
					cameraRoll = cameraPlayer->GetRelativeRotation().Roll + lastDeltaTime * cameraFlowRecoverySpeed.X;
					if (cameraRoll > initialCameraRotation.Roll)
					{
						cameraRoll = cameraPlayer->GetRelativeRotation().Roll + (initialCameraRotation.Roll - cameraPlayer->GetRelativeRotation().Roll) * lastDeltaTime;
					}
				}
				if (bIsTurningOnMovement)
				{
					autoFollowingTimer = 0;
				}

				cameraPlayer->SetRelativeRotation(FRotator(cameraPlayer->GetRelativeRotation().Pitch, cameraYaw, cameraRoll));
			}

			// Get Camera Forward Vector
			FVector cameraForward = (cameraPlayer->GetForwardVector() * FVector(1, 1, 0)).GetSafeNormal();

			// Add Input Vectors to Character Movement
			characterMovement->AddInputVector(cameraForward * value * moveSpeedMultiplier);
		}
		if (bIsGrabbingLedge && value < 0)
		{
			bIsGrabbingLedge = false;
			characterMovement->MovementMode = EMovementMode::MOVE_Falling;
			characterMovement->GravityScale = gravityScale;
			ledgeTimer = 0;
		}

		float cameraPitch = initialCameraRotation.Pitch;

		if (bIsEnableCameraFlow)
		{
			if (value > 0)
			{
				cameraPitch = cameraPlayer->GetRelativeRotation().Pitch + lastDeltaTime * cameraFlowSpeed.Y * value;
				if (cameraPitch > initialCameraRotation.Pitch + cameraFlowAmplitude.Y * value / 1.5f)
				{
					cameraPitch = cameraPlayer->GetRelativeRotation().Pitch + (initialCameraRotation.Pitch + cameraFlowAmplitude.Y * value - cameraPlayer->GetRelativeRotation().Pitch) * lastDeltaTime;
				}
				cameraPlayer->SetRelativeRotation(FRotator(cameraPitch, cameraPlayer->GetRelativeRotation().Yaw, cameraPlayer->GetRelativeRotation().Roll));
			}
			else if (value < 0)
			{
				cameraPitch = cameraPlayer->GetRelativeRotation().Pitch + lastDeltaTime * cameraFlowSpeed.Y * value;
				if (cameraPitch < initialCameraRotation.Pitch + cameraFlowAmplitude.Y * value / 1.5f)
				{
					cameraPitch = cameraPlayer->GetRelativeRotation().Pitch + (initialCameraRotation.Pitch + cameraFlowAmplitude.Y * value - cameraPlayer->GetRelativeRotation().Pitch) * lastDeltaTime;
				}
				cameraPlayer->SetRelativeRotation(FRotator(cameraPitch, cameraPlayer->GetRelativeRotation().Yaw, cameraPlayer->GetRelativeRotation().Roll));
			}
		}
	}
}

// Process right/left input
void APlayerBase::MoveRight(float value)
{

	if (Controller != NULL)
	{
		rightInput += value;
		if (!bIsDead && !bIsLeaping & !bIsClimbing && !bIsJumpingOverObstacle && !bIsGrabbingLedge)
		{
			if (value != 0 && !bIsAbsorbing)
			{
				float cameraPitch = initialCameraRotation.Pitch;
				if (cameraPlayer->GetRelativeRotation().Pitch > initialCameraRotation.Pitch)
				{
					cameraPitch = cameraPlayer->GetRelativeRotation().Pitch - lastDeltaTime * cameraFlowRecoverySpeed.Y;
					if (cameraPitch < initialCameraRotation.Pitch)
					{
						cameraPitch = cameraPlayer->GetRelativeRotation().Pitch + (initialCameraRotation.Pitch - cameraPlayer->GetRelativeRotation().Pitch) * lastDeltaTime;
					}
				}
				else if (cameraPlayer->GetRelativeRotation().Pitch < initialCameraRotation.Pitch)
				{
					cameraPitch = cameraPlayer->GetRelativeRotation().Pitch + lastDeltaTime * cameraFlowRecoverySpeed.Y;
					if (cameraPitch > initialCameraRotation.Pitch)
					{
						cameraPitch = cameraPlayer->GetRelativeRotation().Pitch + (initialCameraRotation.Pitch - cameraPlayer->GetRelativeRotation().Pitch) * lastDeltaTime;
					}
				}
				if (bIsTurningOnMovement)
				{
					autoFollowingTimer = 0;
					cameraTurningTimer += lastDeltaTime;
				}

				cameraPlayer->SetRelativeRotation(FRotator(cameraPitch, cameraPlayer->GetRelativeRotation().Yaw, cameraPlayer->GetRelativeRotation().Roll));
			}
			else
			{
				cameraTurningTimer = 0;
			}

			// Get Camera Right Vector
			FVector cameraForward = (cameraPlayer->GetForwardVector() * FVector(1, 1, 0)).GetSafeNormal();
			FVector cameraRight = FVector::CrossProduct(cameraForward, FVector(0, 0, -1)).GetSafeNormal();

			// Add Input Vectors to Character Movement
			characterMovement->AddInputVector(cameraRight * value * moveSpeedMultiplier);

			if (cameraSpring != NULL && bIsTurningOnMovement && cameraTurningTimer >= turningDelay && !bIsAbsorbing)
			{
				cameraSpring->AddRelativeRotation(FRotator(0, value * turningSpeed / 2.0f * lastDeltaTime, 0));
			}

			float cameraYaw = initialCameraRotation.Yaw;
			float cameraRoll = initialCameraRotation.Roll;

			if (bIsEnableCameraFlow)
			{
				if (value > 0)
				{
					cameraYaw = cameraPlayer->GetRelativeRotation().Yaw + lastDeltaTime * cameraFlowSpeed.Z * value;
					if (cameraYaw > initialCameraRotation.Yaw + cameraFlowAmplitude.Z * value / 1.5f)
					{
						cameraYaw = cameraPlayer->GetRelativeRotation().Yaw + (initialCameraRotation.Yaw + cameraFlowAmplitude.Z * value - cameraPlayer->GetRelativeRotation().Yaw) * lastDeltaTime;
					}
					cameraRoll = cameraPlayer->GetRelativeRotation().Roll - lastDeltaTime * cameraFlowSpeed.X * value;
					if (cameraRoll < initialCameraRotation.Roll - cameraFlowAmplitude.X * value / 1.5f)
					{
						cameraRoll = cameraPlayer->GetRelativeRotation().Roll + (initialCameraRotation.Roll - cameraFlowAmplitude.X * value - cameraPlayer->GetRelativeRotation().Roll) * lastDeltaTime;
					}
					if (bIsTurningOnMovement)
					{
						autoFollowingTimer = 0;
					}
					cameraPlayer->SetRelativeRotation(FRotator(cameraPlayer->GetRelativeRotation().Pitch, cameraYaw, cameraRoll));
				}
				else if (value < 0)
				{
					cameraYaw = cameraPlayer->GetRelativeRotation().Yaw + lastDeltaTime * cameraFlowSpeed.Z * value;
					if (cameraYaw < initialCameraRotation.Yaw + cameraFlowAmplitude.Z * value / 1.5f)
					{
						cameraYaw = cameraPlayer->GetRelativeRotation().Yaw + (initialCameraRotation.Yaw + cameraFlowAmplitude.Z * value - cameraPlayer->GetRelativeRotation().Yaw) * lastDeltaTime;
					}
					cameraRoll = cameraPlayer->GetRelativeRotation().Roll - lastDeltaTime * cameraFlowSpeed.X * value;
					if (cameraRoll > initialCameraRotation.Roll + cameraFlowAmplitude.X * value / 1.5f)
					{
						cameraRoll = cameraPlayer->GetRelativeRotation().Roll + (initialCameraRotation.Roll - cameraFlowAmplitude.X * value - cameraPlayer->GetRelativeRotation().Roll) * lastDeltaTime;
					}
					cameraPlayer->SetRelativeRotation(FRotator(cameraPlayer->GetRelativeRotation().Pitch, cameraYaw, cameraRoll));
				}
			}
		}
	}
}

// Process camera right/left controller input
void APlayerBase::CameraTurn(float value)
{

	cameraRightInput += value;
	if (bIsAbsorbing && bIsReleasing)
	{
		if (focusedPlant != NULL)
		{
			focusedDirectionX += value;
		}
	}
	else
	{
		if (cameraAzimuth != NULL && !bHasTargetLocked)
		{
			if (value != 0)
			{
				autoFollowingTimer = 0;
				bIsFollowingTriggered = false;
			}
			if(cameraPlayer)
			cameraSpring->AddLocalRotation(FRotator(0, value * (cameraSpeed / 1.5f) * lastDeltaTime, 0));
		}
	}
}

// Process camera up/down controller input
void APlayerBase::CameraLookUp(float value)
{
	
	cameraLookUpInput += value;
	if (bIsAbsorbing && bIsReleasing)
	{
		if (focusedPlant != NULL)
		{
			focusedDirectionY -= value;
		}
	}
	else
	{
		/*if (bIsAbsorbing && cameraTPSAzimuth != NULL)
		{
			cameraTPSAzimuth->AddLocalRotation(FRotator(value * (cameraSpeed / 1.75f) * lastDeltaTime, 0, 0));

			// Clamp pitch angle
			float clampedPitchTPS = FMath::ClampAngle(cameraTPSAzimuth->GetRelativeRotation().Pitch, -45, 60);
			cameraTPSAzimuth->SetRelativeRotation(FRotator(clampedPitchTPS, cameraTPSAzimuth->GetRelativeRotation().Yaw, cameraTPSAzimuth->GetRelativeRotation().Roll));
		}*/
		if (cameraSpring != NULL && !bHasTargetLocked)
		{
			if (value != 0)
			{
				autoFollowingTimer = 0;
				bIsFollowingTriggered = false;
			}

			cameraSpring->AddLocalRotation(FRotator(value * (cameraSpeed / 1.75f) * lastDeltaTime, 0, 0));

			// Clamp pitch angle
			float clampedPitch = cameraSpring->GetRelativeRotation().Pitch;
			if (!bIsAbsorbing)
			{
				clampedPitch = FMath::ClampAngle(clampedPitch, cameraMinAngleY, cameraMaxAngleY);
			}
			else
			{
				clampedPitch = FMath::ClampAngle(clampedPitch, aimingCameraMinAngleY, aimingCameraMaxAngleY);
			}

			cameraSpring->SetRelativeRotation(FRotator(clampedPitch, cameraSpring->GetRelativeRotation().Yaw, 0));
		}
	}
}

// Process camera right/left mouse input
void APlayerBase::CameraTurnRate(float value)
{

	cameraRightInput += value;
	if (bIsAbsorbing && bIsReleasing)
	{
		if (focusedPlant != NULL)
		{
			focusedDirectionX += value;
		}
	}
	else
	{
		if (cameraAzimuth != NULL && !bHasTargetLocked)
		{
			if (value != 0)
			{
				autoFollowingTimer = 0;
				bIsFollowingTriggered = false;
			}

			cameraSpring->AddLocalRotation(FRotator(0, value * (cameraSpeed * mouseSensitivity) * lastDeltaTime, 0));
		}
	}
}

// Process camera up/down mouse input
void APlayerBase::CameraLookUpRate(float value)
{
	cameraLookUpInput += value;
	if (bIsAbsorbing && bIsReleasing)
	{
		if (focusedPlant != NULL)
		{
			focusedDirectionY -= value;
		}
	}
	else
	{
		/*if (bIsAbsorbing && cameraTPSAzimuth != NULL)
		{
			cameraTPSAzimuth->AddLocalRotation(FRotator(value * (cameraSpeed / 1.75f) * lastDeltaTime, 0, 0));

			// Clamp pitch angle
			float clampedPitchTPS = FMath::ClampAngle(cameraTPSAzimuth->GetRelativeRotation().Pitch, -45, 60);
			cameraTPSAzimuth->SetRelativeRotation(FRotator(clampedPitchTPS, cameraTPSAzimuth->GetRelativeRotation().Yaw, cameraTPSAzimuth->GetRelativeRotation().Roll));
		}
		else */if (cameraSpring != NULL && !bHasTargetLocked)
		{
			if (value != 0)
			{
				autoFollowingTimer = 0;
				bIsFollowingTriggered = false;
			}

			cameraSpring->AddLocalRotation(FRotator(value * (cameraSpeed * mouseSensitivity) * lastDeltaTime, 0, 0));

			// Clamp pitch angle
			float clampedPitch = cameraSpring->GetRelativeRotation().Pitch;
			if (!bIsAbsorbing)
			{
				clampedPitch = FMath::ClampAngle(clampedPitch, cameraMinAngleY, cameraMaxAngleY);
			}
			else
			{
				clampedPitch = FMath::ClampAngle(clampedPitch, aimingCameraMinAngleY, aimingCameraMaxAngleY);
			}

			cameraSpring->SetRelativeRotation(FRotator(clampedPitch, cameraSpring->GetRelativeRotation().Yaw, 0));

		}
	}
}

#pragma endregion Axis Binds

// --- Action Binds --- //
#pragma region Action Binds

// Process target lock input
void APlayerBase::TargetLockPressed()
{
	if (!bIsDead)
	{

		autoFollowingTimer = 0;

		if (bHasTargetLocked)
		{
			// Unlock current target
			bHasTargetLocked = false;
			currentTarget = NULL;
		}
		else
		{
			// Place camera behind player
			ResetCamera();
		}
	}
}

// Process jump input
void APlayerBase::JumpPressed()
{
	if (bCanJump)
	{
		if (bIsGrabbingLedge)
		{
			bIsGrabbingLedge = false;
			bIsClimbing = true;
			characterMovement->GravityScale = gravityScale;
		}
		else if (!bIsJumpFollowingCurve && !bIsJumpFollowingGravity)
		{
			Jump();
			if (jumpForwardImpulse > 0)
			{
				FVector lastInput = characterMovement->GetLastInputVector();
				lastInput.Normalize();
				characterMovement->AddImpulse(lastInput * jumpForwardImpulse, true);
			}
		}
		else if (!bIsJumping && characterMovement->MovementMode == EMovementMode::MOVE_Walking)
		{
			bIsJumping = true;
			jumpingTimer = 0;
			jumpStartPosition = GetActorLocation();
			fixedJumpDirection = GetActorForwardVector();
			jumpMomentum = characterMovement->GetLastInputVector();
			characterMovement->MovementMode = EMovementMode::MOVE_Falling;
			if (bIsDisableRotationOnJump)
			{
				characterMovement->bOrientRotationToMovement = false;
			}
		}
	}
}

// Process jump release
void APlayerBase::JumpReleased()
{
	if (!bIsJumpFollowingCurve)
	{
		StopJumping();
	}
}

// Process absorb input
void APlayerBase::AbsorbPressed()
{
	if (!bIsDead)
	{
		if (!bIsReleasing)
		{
			flow = TryWaterAbsorption();
			bIsAbsorbing = (flow != EFlowLevel::NONE);

			if (bIsAbsorbing)
			{
				waterbendEffect->Activate(true);
				UsePlayerCamera(true);
			}
		}
	}
}

// Process jump release
void APlayerBase::AbsorbReleased()
{
	if (bIsAbsorbing)
	{
		bIsAbsorbing = false;
		bIsReleasing = false;
		flow = EFlowLevel::NONE;
		waterbendEffect->Deactivate();
		UsePlayerCamera(false);
		if (focusedPlant != NULL)
		{
			focusedPlant->StopTrigger();
			focusedPlant->directionX = 0;
			focusedPlant->directionY = 0;
			focusedPlant->flowLevel = EFlowLevel::NONE;
			focusedPlant = NULL;
		}
	}
}

// Process release input
void APlayerBase::ReleasePressed()
{
	if (!bIsDead)
	{
		if (bIsAbsorbing)
		{
			bIsReleasing = true;
			if (focusedPlant != NULL)
			{
				focusedPlant->directionX = 0;
				focusedPlant->directionY = 0;
				if (focusedPlant->chooseAngle)
				{
					focusedPlant->TargetLockInteractible(this);
					cameraSpring->AddWorldRotation(UKismetMathLibrary::NormalizedDeltaRotator((focusedPlant->GetActorLocation() - cameraPlayer->GetComponentLocation()).Rotation(), cameraPlayer->GetComponentRotation()), true);
				}
				else
				{
					WaterBend(focusedPlant);
					bIsReleasing = false;
				}

			}
			else
			{
				WaterBend(NULL);
				bIsReleasing = false;
			}
		}
		else
		{
			bIsReleasing = true;
			UsePlayerCamera(true);
		}
	}
}

// Process release release
void APlayerBase::ReleaseReleased()
{
	if (bIsAbsorbing) 
	{
		if (bIsReleasing)
		{
			if (focusedPlant != NULL)
			{
				float directionRate = UKismetMathLibrary::Abs(focusedPlant->directionX) + UKismetMathLibrary::Abs(focusedPlant->directionY);
				if (directionRate >= 0.5f)
				{
					//focusedPlant->ProcessInput(this, true);
					WaterBend(focusedPlant);
					focusedPlant->StopTrigger();
					focusedPlant = NULL;
				}
				else
				{
					focusedPlant->directionX = 0;
					focusedPlant->directionY = 0;
					focusedPlant->flowLevel = EFlowLevel::NONE;
				}
			}
		}
	}
	else if(bIsReleasing)
	{
		UsePlayerCamera(false);
	}
	bIsReleasing = false;
}

// Process launch dialogue input
void APlayerBase::LaunchDialoguePressed()
{
	if (this->PNJConversation)
	{
		if (APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0))
		{
			PC->Possess(this->PNJConversation);
		}
	}
}

#pragma endregion Action Binds

// --- Delay Functions --- //
#pragma region Delay Functions

void APlayerBase::DelayDisableCameraLag()
{
	if (bIsAbsorbing)
	{
		//cameraSpring->bEnableCameraLag = false;
		cameraSpring->bEnableCameraRotationLag = false;
	}
}

#pragma endregion Delay Functions

// --- Events Processing --- //
#pragma region Events Processing

// Process jump execution
void APlayerBase::ProcessJump()
{
	if ((jumpingTimer >= 0.1f && characterMovement->MovementMode == EMovementMode::MOVE_Walking) 
		|| (bIsJumpFollowingCurve && jumpingTimer > jumpCurve->FloatCurve.GetLastKey().Time)
		|| (bIsJumpFollowingGravity && jumpingTimer > gravityCurve->FloatCurve.GetLastKey().Time))
	{
		bIsJumping = false;
		characterMovement->GravityScale = gravityScale;
	}
	else if (bIsJumping)
	{
		if (bIsJumpFollowingCurve)
		{
			if (fixedJumpDistance > 0 && jumpMomentum.Size() >= fixedJumpThreshold)
			{
				SetActorLocation(FVector(jumpStartPosition.X + fixedJumpDirection.X * fixedJumpDistance * jumpingTimer, jumpStartPosition.Y + fixedJumpDirection.Y * fixedJumpDistance * jumpingTimer, jumpStartPosition.Z + jumpCurve->GetFloatValue(jumpingTimer) * jumpCurveMagnitude), true);
			}
			else
			{
				FVector currentLocation = GetActorLocation();
				SetActorLocation(FVector(currentLocation.X, currentLocation.Y, jumpStartPosition.Z + jumpCurve->GetFloatValue(jumpingTimer) * jumpCurveMagnitude), true);
			}
		}
		else if (bIsJumpFollowingGravity)
		{
			characterMovement->GravityScale = -gravityCurve->GetFloatValue(jumpingTimer) * gravityCurveMagnitude;
			if (fixedJumpDistance > 0 && jumpMomentum.Size() >= fixedJumpThreshold)
			{
				SetActorLocation(FVector(jumpStartPosition.X + fixedJumpDirection.X * fixedJumpDistance * jumpingTimer, jumpStartPosition.Y + fixedJumpDirection.Y * fixedJumpDistance * jumpingTimer, GetActorLocation().Z), true);
			}
		}
	}
}

// Process player behaviour on a ledge
void APlayerBase::ProcessLedge()
{
	ledgeTimer += lastDeltaTime;
	if (bIsGrabbingLedge)
	{
		characterMovement->Velocity = FVector::ZeroVector;
		characterMovement->MovementMode = EMovementMode::MOVE_Flying;
	}
	else if (bIsLeaping || bIsJumpingOverObstacle || bIsClimbing)
	{
		FollowSpline(lastDeltaTime);
	}
	else
	{
		splineTimer = 0;
		FSplinePoint begin = FSplinePoint();
		FSplinePoint middle = FSplinePoint();
		FSplinePoint end = FSplinePoint();
		FVector leapDirection;
		float forwardDelta = 0;
		switch (CheckLedge())
		{
		case EObstacle::DOWN:
			leapDirection = (ledgeDirection - GetActorLocation()) * FVector(1, 1, 0);
			leapDirection.Normalize();
			forwardDelta = UKismetMathLibrary::Abs(FVector::DotProduct(characterMovement->GetLastInputVector(), leapDirection) - 1);
			if (characterMovement->MovementMode == EMovementMode::MOVE_Walking && characterMovement->GetLastInputVector() != FVector::ZeroVector && forwardDelta < 0.5f)
			{
				contextualSpline->ClearSplinePoints();
				begin = FSplinePoint(0, GetActorLocation(), ESplinePointType::CurveCustomTangent);
				end = FSplinePoint(1, ledgeDirection + FVector(0, 0, 5), ESplinePointType::CurveCustomTangent);
				begin.LeaveTangent = FVector(0, 0, leapTengent.X * ((FVector::Distance(GetActorLocation(), ledgeDirection)) / leapStandardDistance));
				end.ArriveTangent = FVector(0, 0, -leapTengent.Y * ((FVector::Distance(GetActorLocation(), ledgeDirection)) / leapStandardDistance));
				contextualSpline->AddPoint(begin);
				contextualSpline->AddPoint(end);
				contextualSpline->Duration = leapDuration * ((FVector::Distance(GetActorLocation(), ledgeDirection)) / leapStandardDistance);
				SetActorRotation(((ledgeDirection - GetActorLocation()) * FVector(1, 1, 0)).Rotation());
				bIsLeaping = true;
			}
			break;
		case EObstacle::MIDDLE:
			contextualSpline->ClearSplinePoints();
			begin = FSplinePoint(0, GetActorLocation(), ESplinePointType::Linear);
			middle = FSplinePoint(1, FVector(GetActorLocation().X, GetActorLocation().Y, ledgeDirection.Z - 20), ESplinePointType::CurveCustomTangent);
			end = FSplinePoint(2, ledgeDirection + FVector(0, 0, 5), ESplinePointType::CurveCustomTangent);
			middle.LeaveTangent = FVector(0, 0, jumpOverObstacleTengent.X);
			end.ArriveTangent = FVector(0, 0, -jumpOverObstacleTengent.Y);
			contextualSpline->AddPoint(begin);
			contextualSpline->AddPoint(middle);
			contextualSpline->AddPoint(end);
			contextualSpline->Duration = jumpOverObstacleDuration;
			SetActorRotation(((ledgeDirection - GetActorLocation()) * FVector(1, 1, 0)).Rotation());
			bIsJumpingOverObstacle = true;
			break;
		case EObstacle::UP:
			if (characterMovement->MovementMode == EMovementMode::MOVE_Falling && (bAutoClimb || ledgeTimer >= 0.75f))
			{
				contextualSpline->ClearSplinePoints();
				begin = FSplinePoint(0, FVector(GetActorLocation().X, GetActorLocation().Y, ledgeDirection.Z - characterHeight), ESplinePointType::Linear);
				middle = FSplinePoint(1, FVector(GetActorLocation().X, GetActorLocation().Y, ledgeDirection.Z - 20), ESplinePointType::CurveCustomTangent);
				end = FSplinePoint(2, ledgeDirection + FVector(0, 0, 5), ESplinePointType::CurveCustomTangent);
				middle.LeaveTangent = FVector(0, 0, climbTengent.X);
				end.ArriveTangent = FVector(0, 0, -climbTengent.Y);
				contextualSpline->AddPoint(begin);
				contextualSpline->AddPoint(middle);
				contextualSpline->AddPoint(end);
				contextualSpline->Duration = climbDuration;
				SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, ledgeDirection.Z - characterHeight), false);
				SetActorRotation(((ledgeDirection - GetActorLocation()) * FVector(1, 1, 0)).Rotation());
				if (bAutoClimb)
				{
					bIsClimbing = true;
				}
				else
				{
					characterMovement->GravityScale = 0;
					bIsGrabbingLedge = true;
				}
			}
			break;
		}
	}
}

#pragma endregion Events Processing

// --- State Control --- //
#pragma region State Control

#pragma endregion State Control

// --- Utility Functions --- //
#pragma region Utility Functions

// Rotate player mesh to face locked target
void APlayerBase::FaceTarget()
{
	if (currentTarget != NULL)
	{
		FRotator newRotation = FRotator(0, (currentTarget->GetActorLocation() - GetActorLocation()).Rotation().Yaw - 90, 0);
		GetMesh()->SetWorldRotation(newRotation);
	}
}

// Move player along contextual spline
void APlayerBase::FollowSpline(float deltaTime)
{
	splineTimer += deltaTime;
	if (splineTimer <= contextualSpline->Duration)
	{
		SetActorLocation(contextualSpline->GetLocationAtTime(splineTimer, ESplineCoordinateSpace::Local));
	}
	else
	{
		bIsClimbing = false;
		bIsJumpingOverObstacle = false;
		bIsLeaping = false;
		contextualSpline->ClearSplinePoints(true);
		characterMovement->MovementMode = EMovementMode::MOVE_Falling;
	}
}

// Detect plants in range of player
void APlayerBase::DetectPlants()
{
	if (bIsAbsorbing || bIsReleasing)
	{
		APlant* newPlant = CheckTarget();
		if (focusedPlant != NULL)
		{
			if (newPlant == NULL || focusedPlant->GetActorLocation() != newPlant->GetActorLocation())
			{
				focusedPlant->StopTrigger();
				focusedPlant->directionX = 0;
				focusedPlant->directionY = 0;
				focusedPlant->flowLevel = EFlowLevel::NONE;
			}
		}
		focusedPlant = newPlant;
		if (focusedPlant != NULL)
		{
			focusedPlant->TriggerInteractible(this);
			focusedPlant->flowLevel = flow;
		}
	}
}

// Focus camera on target
void APlayerBase::TriggerTarget(AActor* target)
{
	if (!bIsDead)
	{

		autoFollowingTimer = 0;

		if (target == NULL)
		{
			// Unlock current target
			bHasTargetLocked = false;
			currentTarget = NULL;
		}
		else
		{
			// Lock first enemy in range
			currentTarget = target;
			bHasTargetLocked = true;
		}
	}
}

#pragma endregion Utility Functions