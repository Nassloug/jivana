// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "Components/SplineComponent.h"
#include "Engine/DataTable.h"
#include "ContextualCamera.generated.h"

/**
 * 
 */
UCLASS()
class JIVANA_API AContextualCamera : public ACameraActor
{
	GENERATED_BODY()

public:

    void UpdateCamera();

    // Read contextual camera data table and initialize parameters
    UFUNCTION(BlueprintCallable, Category = "Controller")
        void InitializeContextualCameraType();

    AActor* target;

    UPROPERTY(Category = "Trail", EditAnywhere, BlueprintReadWrite)
        USplineComponent* cameraTrail;

protected:

    UPROPERTY(Category = "Data", EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
        UDataTable* cameraParameters;

    UPROPERTY(Category = "Data", EditAnywhere, BlueprintReadWrite)
        FString cameraType;

private:

    bool bIsControlledBySequence;

    bool constraintPositionX;
    bool constraintPositionY;
    bool constraintPositionZ;

    FVector positionOffset;

    bool constraintRotationPitch;
    bool constraintRotationYaw;
    bool constraintRotationRoll;

    bool followTargetWithPosition;
    bool followTargetWithRotation;

    bool followTrail;

};
