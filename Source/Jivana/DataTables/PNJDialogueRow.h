// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PNJDialogueRow.generated.h"

USTRUCT(BlueprintType)
struct FPNJDialogueRow : public FTableRowBase
{
    GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FDataTableRowHandle PNJ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Sentence;
};
