// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ContextualCameraRow.generated.h"

USTRUCT(BlueprintType)
struct FContextualCameraRow : public FTableRowBase
{
    GENERATED_BODY()

public:

    UPROPERTY(Category = "Cinematic", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Let sequencer handle camera behaviour."))
        bool bIsControlledBySequence;

    UPROPERTY(Category = "Position", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Block position change on X axis."))
        bool constraintPositionX;

    UPROPERTY(Category = "Position", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Block position change on Y axis."))
        bool constraintPositionY;

    UPROPERTY(Category = "Position", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Block position change on Z axis."))
        bool constraintPositionZ;

    UPROPERTY(Category = "Position", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Position offset from target."))
        FVector positionOffset;

    UPROPERTY(Category = "Rotation", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Block position change on Pitch axis."))
        bool constraintRotationPitch;
    
    UPROPERTY(Category = "Rotation", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Block position change on Yaw axis."))
        bool constraintRotationYaw;
    
    UPROPERTY(Category = "Rotation", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Block position change on Roll axis."))
        bool constraintRotationRoll;

    UPROPERTY(Category = "Tracking", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Update position following target."))
        bool followTargetWithPosition;
    
    UPROPERTY(Category = "Tracking", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Update rotation following target."))
        bool followTargetWithRotation;

    UPROPERTY(Category = "Tracking", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Update position following camera spline."))
        bool followTrail;

};
