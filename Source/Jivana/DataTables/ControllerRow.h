// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ControllerRow.generated.h"

USTRUCT(BlueprintType)
struct FControllerRow : public FTableRowBase
{
    GENERATED_BODY()
public:
	// --- Camera Properties --- //

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite)
		float cameraSpeed;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera distance from player."))
		float cameraDistance;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Initial camera spring position."))
		FVector cameraSpringPosition;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera offset from spring."))
		FVector cameraPositionOffset;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Initial camera rotation."))
		FRotator cameraRotation;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Lower value for more lag."))
		float cameraLag;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Lower value for more lag."))
		float cameraRotationLag;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite)
		float mouseSensitivity;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera max limit on Y axis."))
		float cameraMaxAngleY;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera min limit on Y axis."))
		float cameraMinAngleY;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Rotate player towards aiming direction."))
		bool characterFollowAiming;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera max limit on Y axis while aiming."))
		float aimingCameraMaxAngleY;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera min limit on Y axis while aiming."))
		float aimingCameraMinAngleY;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Is the camera turning with left and right inputs."))
		bool bIsTurningOnMovement;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Speed of the camera turning on left/right inputs."))
		float turningSpeed;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Delay without camera inputs before triggering turning behaviour."))
		float turningDelay;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Is the camera trying to stay behind player after a delay without camera inputs."))
		bool bIsAutoFollowing;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Speed of he camera auto following behaviour."))
		float autoFollowingSpeed;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Delay without camera inputs before triggering auto following behaviour. If set to 0, camera will always try to stay behind player."))
		float autoFollowingDelay;

	UPROPERTY(Category = "Camera", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Minimum range required between camera angle and forward angle to trigger auto following. Value is relevant between 0 and 2."))
		float autoFollowingMinRange;

	UPROPERTY(Category = "Camera: Flow", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Enable camera flow effects."))
		bool bIsEnableCameraFlow;

	UPROPERTY(Category = "Camera: Flow", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera flow speed for all axis."))
		FVector cameraFlowSpeed;

	UPROPERTY(Category = "Camera: Flow", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera flow recovery speed for all axis."))
		FVector cameraFlowRecoverySpeed;

	UPROPERTY(Category = "Camera: Flow", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Camera flow amplitude for all axis"))
		FVector cameraFlowAmplitude;

	// --- Target Tracking Properties --- //

	UPROPERTY(Category = "Target Tracking", EditAnywhere, BlueprintReadWrite)
		float maxTrackingDistance;

	UPROPERTY(Category = "Target Tracking", EditAnywhere, BlueprintReadWrite)
		float trackingLag;

	UPROPERTY(Category = "Target Tracking", EditAnywhere, BlueprintReadWrite)
		float trackingRotationLag;

	// --- Level Properties --- //

	UPROPERTY(Category = "Level", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Max distance from the center of the screen in which targets are detected"))
		float targetAnchorDistance;

	// --- Character Properties --- //
	
	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Character's max speed."))
		float characterMaxSpeed;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Character's acceleration."))
		float characterAcceleration;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Character's height for calculations."))
		float characterHeight;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Is the character able to jump."))
		bool bCanJump;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Magnitude of the jump impulse if not following curve."))
		float jumpForce;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Magnitude of the jump forward impulse if not following curve."))
		float jumpForwardImpulse;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Constant gravity affecting character"))
		float gravityScale;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Amount of movement control when falling or jumping."))
		float airControl;

	UPROPERTY(Category = "Character", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Disable player rotation while jumping."))
		bool bIsDisableRotationOnJump;

	// --- Jump Curve --- //

	UPROPERTY(Category = "Jump: Curve", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "(True) Jump height follow the jump curve. (False) Jump is impulse or gravity curve."))
		bool bIsJumpFollowingCurve;

	UPROPERTY(Category = "Jump: Curve", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Control the height of the jump if IsJumpFollowingCurve is true."))
		UCurveFloat* jumpCurve;

	UPROPERTY(Category = "Jump: Curve", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Magnitude of the jump curve or the impulse."))
		float jumpCurveMagnitude;

	UPROPERTY(Category = "Jump: Curve", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "If > 0, fix the jump distance. This disables air control during the curve evaluation."))
		float fixedJumpDistance;

	UPROPERTY(Category = "Jump: Curve", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Movement threshold to trigger jump distance. If the movement magitude is < to the threshold, character stays in its XY position while jumping."))
		float fixedJumpThreshold;

	// --- Jump Gravity --- //

	UPROPERTY(Category = "Jump: Gravity", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "(True) Jump is simulated by gravity change. (False) Jump is impulse or jump curve."))
		bool bIsJumpFollowingGravity;

	UPROPERTY(Category = "Jump: Gravity", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Control the gravity over time."))
		UCurveFloat* gravityCurve;

	UPROPERTY(Category = "Jump: Gravity", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Multiply gravity curve magnitude."))
		float gravityCurveMagnitude;

	// --- Ledge Behaviour --- //

	UPROPERTY(Category = "Ledge: Leap", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Standard distance of a leap during leapDuration time."))
		float leapStandardDistance;

	UPROPERTY(Category = "Ledge: Leap", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Constant duration of a leap in seconds, relative to leapStandardDistance."))
		float leapDuration;

	UPROPERTY(Category = "Ledge: Leap", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "X is begin tengent, Y is end tengent for a leap."))
		FVector2D leapTengent;

	UPROPERTY(Category = "Ledge: Leap", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Position offset for the box cast detecting small obstacles."))
		FVector2D minDetectionOffset;

	UPROPERTY(Category = "Ledge: Leap", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Extents for box cast detecting small obstacles."))
		FVector minDetectionExtents;

	UPROPERTY(Category = "Ledge: Leap", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Position offset for the box cast detecting auto jump ledge."))
		FVector2D autoJumpDetectionOffset;

	UPROPERTY(Category = "Ledge: Leap", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Extents for the box cast detecting auto jump ledge."))
		FVector autoJumpDetectionExtents;

	UPROPERTY(Category = "Ledge: Jump Over", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Constant duration of a jump over an obstacle in seconds."))
		float jumpOverObstacleDuration;

	UPROPERTY(Category = "Ledge: Jump Over", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "X is begin tengent, Y is end tengent for a jump over an obstacle."))
		FVector2D jumpOverObstacleTengent;

	UPROPERTY(Category = "Ledge: Jump Over", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Position offset for the box cast detecting medium obstacles."))
		FVector2D midDetectionOffset;

	UPROPERTY(Category = "Ledge: Jump Over", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Extents for box cast detecting medium obstacles."))
		FVector midDetectionExtents;

	UPROPERTY(Category = "Ledge: Climb", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "If false, climbing needs input from player."))
		bool bAutoClimb;

	UPROPERTY(Category = "Ledge: Climb", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Constant duration of climbing in seconds."))
		float climbDuration;

	UPROPERTY(Category = "Ledge: Climb", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "X is begin tengent, Y is end tengent for climbing."))
		FVector2D climbTengent;

	UPROPERTY(Category = "Ledge: Climb", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Position offset for the box cast detecting high obstacles."))
		FVector2D maxDetectionOffset;

	UPROPERTY(Category = "Ledge: Climb", EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "Extents for box cast detecting high obstacles."))
		FVector maxDetectionExtents;
};
