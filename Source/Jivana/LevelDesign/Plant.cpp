// Fill out your copyright notice in the Description page of Project Settings.


#include "Plant.h"

// Sets default values
APlant::APlant()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlant::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlant::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CustomTimeDilation = (1.0f / GetWorldSettings()->GetEffectiveTimeDilation());

}

//// Called on player input
//void APlant::ProcessInput_Implementation(ACharacter* player, bool activate)
//{
//
//}
//
//// Highlight plant on player target
//void APlant::TriggerPlant_Implementation(ACharacter* player)
//{
//
//}
//
//// Stop highlighting plant
//void APlant::StopTrigger_Implementation()
//{
//
//}
//
//// Called on plant target lock
//void APlant::TargetLockPlant_Implementation(ACharacter* player)
//{
//
//}

