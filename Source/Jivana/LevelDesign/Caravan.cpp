// Fill out your copyright notice in the Description page of Project Settings.


#include "Caravan.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ACaravan::ACaravan()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACaravan::BeginPlay()
{
	Super::BeginPlay();

	currentPosition = 0;
}

// Called every frame
void ACaravan::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	FollowSpline(DeltaTime);
}

// Update caravan position on spline
void ACaravan::FollowSpline(float deltaTime)
{
	if (caravanTrail != NULL && caravanMesh != NULL)
	{
		currentPosition = UKismetMathLibrary::FMin(currentPosition + deltaTime * caravanSpeed, caravanLimit);
		if (currentPosition <= caravanTrail->Duration)
		{
			caravanMesh->SetRelativeRotation((caravanTrail->GetLocationAtTime(currentPosition, ESplineCoordinateSpace::Local) - caravanMesh->GetRelativeLocation()).Rotation());
			caravanMesh->SetRelativeLocation(caravanTrail->GetLocationAtTime(currentPosition, ESplineCoordinateSpace::Local));
		}
	}
}

