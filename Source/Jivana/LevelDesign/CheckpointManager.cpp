// Fill out your copyright notice in the Description page of Project Settings.


#include "CheckpointManager.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "JivanaGameSettings.h"
#include "../Controller/PlayerBase.h"

// Sets default values
ACheckpointManager::ACheckpointManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACheckpointManager::BeginPlay()
{
	Super::BeginPlay();

	/*const auto& Settings = GetMutableDefault<UJivanaGameSettings>();
	const auto& ClassToFind = Settings->CheckpointActor;

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassToFind, FoundActors);

	for (const auto& actor : FoundActors)
	{
		FString str;
		actor->GetName(str);
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Result: %s"), *str));
	}*/
}

// Called every frame
void ACheckpointManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACheckpointManager::TraverseCheckpoint(AActor* actor)
{
	m_LastCheckpointTraversed = actor;
}

void ACheckpointManager::RespawnPlayerOnLastCheckpoint(APlayerBase* player, bool& success)
{
	if ((m_LastCheckpointTraversed != nullptr) && (player != nullptr))
	{
		FVector destination = m_LastCheckpointTraversed->GetActorLocation();
		FRotator rotator = m_LastCheckpointTraversed->GetActorRotation();
		player->TeleportTo(destination, rotator, false, false);

		success = true;
		return;
	}

	success = false;
	return;
}
