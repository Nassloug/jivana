// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueManager.h"

#include "Engine/DataTable.h"
#include "../DataTables/PNJDialogueRow.h"

// Sets default values
ADialogueManager::ADialogueManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ADialogueManager::BeginPlay()
{
	Super::BeginPlay();

	m_IsDialogueStarted = false;
}

// Called every frame
void ADialogueManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool ADialogueManager::LaunchDialogue(const FName& Name, const UDataTable* DataTable)
{
	if (m_IsDialogueStarted)
		return false;

	FString ContextString(TEXT("Dialogue"));
	FPNJDialogueRow* dialogue = DataTable->FindRow<FPNJDialogueRow>(Name, ContextString, true);

	if (dialogue == nullptr)
		return false;

	UE_LOG(LogTemp, Log, TEXT("DataTable contains name"));
	m_IsDialogueStarted = true;

	return true;
}

bool ADialogueManager::IsDialogueStarted() const
{
	return m_IsDialogueStarted;
}
