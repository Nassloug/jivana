// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractibleObject.h"

// Sets default values
AInteractibleObject::AInteractibleObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AInteractibleObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInteractibleObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AInteractibleObject::ActivateInteractible_Implementation()
{

}


void AInteractibleObject::ProcessInput_Implementation(ACharacter* player, bool activate)
{

}


void AInteractibleObject::TriggerInteractible_Implementation(ACharacter* player)
{

}


void AInteractibleObject::StopTrigger_Implementation()
{

}


void AInteractibleObject::TargetLockInteractible_Implementation(ACharacter* player)
{

}

