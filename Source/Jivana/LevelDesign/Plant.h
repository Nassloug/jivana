// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Jivana.h"
#include "../Controller/ContextualCamera.h"
#include "GameFramework/Actor.h"
#include "InteractibleObject.h"

#include "Plant.generated.h"

UCLASS()
class JIVANA_API APlant : public AInteractibleObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlant();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//// Called on player input
	//UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
	//	void ProcessInput(ACharacter* player, bool activate);

	//// Highlight plant on player target
	//UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
	//	void TriggerPlant(ACharacter* player);

	//// Stop highlighting plant
	//UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
	//	void StopTrigger();

	//// Called on plant target lock
	//UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
	//	void TargetLockPlant(ACharacter* player);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector2D targetPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USceneComponent* targetTracker;

	UPROPERTY(Category = "Effect", BlueprintReadWrite)
		float directionX;

	UPROPERTY(Category = "Effect", BlueprintReadWrite)
		float directionY;

	UPROPERTY(Category = "Effect",  BlueprintReadWrite)
		EFlowLevel flowLevel;

	UPROPERTY(Category = "Effect", BlueprintReadWrite)
		bool bIsActive;

	UPROPERTY(Category = "Effect",  BlueprintReadWrite)
		AContextualCamera* focusCamera;

	// --- Common Variables --- //
	
	float distanceFromCenter;
};
