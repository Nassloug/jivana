// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SplineComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Caravan.generated.h"

UCLASS()
class JIVANA_API ACaravan : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACaravan();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(Category = "Level", EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* caravanMesh;

	UPROPERTY(Category = "Level", EditAnywhere, BlueprintReadWrite)
		USplineComponent* caravanTrail;

	UPROPERTY(Category = "Level", EditAnywhere, BlueprintReadWrite)
		float caravanSpeed;

	UPROPERTY(Category = "Level", EditAnywhere, BlueprintReadWrite)
		float caravanLimit;

private:

	// Update caravan position on spline
	void FollowSpline(float deltaTime);

	float currentPosition;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
