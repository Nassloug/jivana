// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractibleObject.generated.h"

UCLASS()
class JIVANA_API AInteractibleObject : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AInteractibleObject();

	UPROPERTY(Category = "Interactible", BlueprintReadWrite, EditAnywhere)
		bool chooseAngle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractibleObject")
		void ActivateInteractible();

	// Called on player input
	UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
		void ProcessInput(ACharacter* player, bool activate);

	// Highlight interactible on player target
	UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
		void TriggerInteractible(ACharacter* player);

	// Stop highlighting plant
	UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
		void StopTrigger();

	// Called on interactible target lock
	UFUNCTION(BlueprintNativeEvent, Category = "Interactions")
		void TargetLockInteractible(ACharacter* player);


};
