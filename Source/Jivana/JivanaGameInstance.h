// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Misc/DateTime.h"
#include "JivanaGameInstance.generated.h"

class ACheckpointManager;
class ADialogueManager;

/**
 * 
 */
UCLASS()
class JIVANA_API UJivanaGameInstance : public UGameInstance
{
	GENERATED_BODY()

private:

	FDateTime StartTime;

	ACheckpointManager* m_CheckpointManager = nullptr;
	ADialogueManager* m_DialogueManager = nullptr;

private:

	bool SaveTimeSpentInFile();

public:

	void Init() override;

	void OnWorldChanged(UWorld* OldWorld, UWorld* NewWorld) override;
	void Shutdown() override;

	UFUNCTION(BlueprintPure, Category = "Jivana")
	ACheckpointManager* GetCheckpointManager(bool& success) const
	{
		success = m_CheckpointManager != nullptr;
		return m_CheckpointManager;
	}

	UFUNCTION(BlueprintPure, Category = "Jivana")
	ADialogueManager* GetDialogueManager(bool& success) const
	{
		success = m_DialogueManager != nullptr;
		return m_DialogueManager;
	}

	UFUNCTION(BlueprintCallable, Category = "Jivana")
		FString GetTimeSpent();
};
