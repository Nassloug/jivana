// Fill out your copyright notice in the Description page of Project Settings.


#include "JivanaGameInstance.h"

#include "LevelDesign/CheckpointManager.h"
#include "LevelDesign/DialogueManager.h"

#include "HAL/PlatformFilemanager.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"

// Add (|| PLATFORM_MAC) to enable Mac to Mac remote building
#define UNIXLIKE_TO_MAC_REMOTE_BUILDING (PLATFORM_LINUX)

bool UJivanaGameInstance::SaveTimeSpentInFile()
{
	FString textItem = this->GetTimeSpent();

	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

#if PLATFORM_MAC || UNIXLIKE_TO_MAC_REMOTE_BUILDING
	FString OutputDir = TEXT("/tmp");
#else
	FString OutputDir = FPlatformProcess::UserDir();
#endif

	FString filepath = FPaths::Combine(OutputDir, FString::Printf(TEXT("%s"), TEXT("jivana-time-spent.txt")));
	UE_LOG(LogTemp, Log, TEXT("Write jivana-time-spent file at %s"), *filepath);

	if (!FFileHelper::SaveStringToFile(textItem, *filepath))
		return false;

	return true;
}

void UJivanaGameInstance::Init()
{
	this->StartTime = FDateTime::UtcNow();
}

void UJivanaGameInstance::OnWorldChanged(UWorld* OldWorld, UWorld* NewWorld)
{
	if (!NewWorld)
		return;

	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	m_CheckpointManager = NewWorld->SpawnActor<ACheckpointManager>(Location, Rotation, SpawnInfo);
	m_DialogueManager = NewWorld->SpawnActor<ADialogueManager>(Location, Rotation, SpawnInfo);
}

void UJivanaGameInstance::Shutdown()
{
	this->SaveTimeSpentInFile();
}

FString UJivanaGameInstance::GetTimeSpent()
{
	FDateTime EndTime = FDateTime::UtcNow();

	FTimespan diff = EndTime - this->StartTime;

	FString textItem;
	diff.ExportTextItem(textItem, diff, nullptr, 0, nullptr);

	return textItem;
}
