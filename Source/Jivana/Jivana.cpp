// Copyright Epic Games, Inc. All Rights Reserved.

#include "Jivana.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Jivana, "Jivana" );
