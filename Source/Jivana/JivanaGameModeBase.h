// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JivanaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JIVANA_API AJivanaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
