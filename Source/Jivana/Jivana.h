// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Jivana.generated.h"

UENUM(BlueprintType)
enum class EFlowLevel : uint8 {
	NONE,
	MINIMAL   UMETA(DisplayName = "POND"),
	MEDIUM    UMETA(DisplayName = "RIVER"),
	MAXIMAL   UMETA(DisplayName = "WATERFALL"),
};

UENUM(BlueprintType)
enum class EObstacle : uint8 {
	NONE,
	DOWN	  UMETA(DisplayName = "DOWN"),
	MIDDLE    UMETA(DisplayName = "MIDDLE"),
	UP		  UMETA(DisplayName = "UP"),
};

struct FTimerHandleContainer
{
	//FTimerHandle handle;
	bool isJivValid;
};

USTRUCT(BlueprintType)
struct FTimerHandleContainerWrapper
{
	GENERATED_USTRUCT_BODY()
public:
	TSharedPtr<FTimerHandleContainer> ptr;
};
