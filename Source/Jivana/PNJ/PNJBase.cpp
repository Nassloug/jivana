// Fill out your copyright notice in the Description page of Project Settings.


#include "PNJBase.h"

// Sets default values
APNJBase::APNJBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APNJBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APNJBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APNJBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

